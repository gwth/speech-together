FROM node:18-alpine

ARG NEXT_PUBLIC_API_ENDPOINT
ARG NEXT_PUBLIC_API_PORT

WORKDIR /app

ENV NEXT_PUBLIC_API_ENDPOINT=$NEXT_PUBLIC_API_ENDPOINT
ENV NEXT_PUBLIC_API_PORT=$NEXT_PUBLIC_API_PORT

RUN echo '-----------------env-----------------'
RUN echo $NEXT_PUBLIC_API_ENDPOINT
RUN echo $NEXT_PUBLIC_API_PORT
RUN env



COPY package.json pnpm-lock.yaml ./

RUN npm install -g pnpm
RUN pnpm install

COPY . .

RUN pnpm run build

EXPOSE 3000

CMD [ "pnpm","run","start" ]