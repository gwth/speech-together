/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  env: {
    MICROSOFT_KEY: process.env.MICROSOFT_KEY,
    MICROSOFT_REGION: process.env.MICROSOFT_REGION,
    // API_ENDPOINT: process.env.NEXT_PUBLIC_API_ENDPOINT,
    // API_PORT: process.env.NEXT_PUBLIC_API_PORT,
  }
}

module.exports = nextConfig
