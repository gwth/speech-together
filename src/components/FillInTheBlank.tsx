import React, { forwardRef, useImperativeHandle, useState } from "react";
import {
  Button,
  CardMedia,
  InputBase,
  TextField,
  Typography,
} from "@mui/material";
import { styled, withStyles } from "@mui/material/styles";
import { useRef } from "react";
interface FillInTheBlankParagraphProps {
  paragraph: string;
  blanks: number;
  answers: string[];
  ref: any;
  pathImg: string;
}
// const WhiteBorderTextField = styled(TextField)`
//   & .MuiOutlinedInput-root {
//     &.Mui-focused fieldset {
//       border-color: blue;
//     }
//     fieldset {
//       border-color: white;
//       variant: standard;
//     }
//   }
// `;
const FillInTheBlankParagraph: React.FC<FillInTheBlankParagraphProps> =
  forwardRef(({ paragraph, blanks, answers, pathImg }, ref) => {
    useImperativeHandle(ref, () => ({
      handleSubmit,
    }));
    const [userAnswers, setUserAnswers] = useState(
      Array.from({ length: blanks }, () => "")
    );

    const handleSubmit = () => {
      let score: number = 0;
      const isCorrect = userAnswers.every((userAnswer, index) => {
        // console.log(userAnswer.trim());
        // if (userAnswer.trim() === answers[index]) score++;
        return userAnswer.trim() === answers[index];
      });
      userAnswers.forEach((userAnswer, index) => {
        if (userAnswer.trim() === answers[index]) {
          score++;
        }
      });
      console.log(
        userAnswers.some((s) => s == "") + " " + blanks + " " + score
      );
      // alert("คะแนนที่ได้ " + score + " คะแนน");
      // return userAnswers.some(s=> s=='') ? score : 99;
      return userAnswers.some((s) => s == "") ? 99 : isCorrect ? 1 : 0;
      // window.location.reload();
      // if (isCorrect) {
      //   alert("Correct answers! " + score);
      // } else {
      //   alert("Incorrect answers. Try again. " + score);
      // }
    };

    const generateParagraphWithInputs = () => {
      const paragraphFragments = paragraph.split("<inputfield>");
      // console.log(paragraph.length);
      // console.log(paragraphFragments);

      return (
        <div>
          {paragraphFragments.map((fragment, index) => {
            return (
              <div
                key={index}
                className="text-lg/10 text-black inline-flex items-center"
              >
                {/* <Typography
                  dangerouslySetInnerHTML={{ __html: fragment }}
                ></Typography> */}
                {/* <div className="inline-flex items-center">{fragment}</div> */}
                {fragment}
                {index !== paragraphFragments.length - 1 && (
                  <>
                    <TextField
                      className="px-2 "
                      size="small"
                      variant="standard"
                      value={userAnswers[index]}
                      inputProps={{
                        maxLength: answers[index].length,
                        style: {
                          textAlign: "center",
                          // color: "white",
                          // borderColor: "red",
                          fontSize: 18,
                        },
                      }}
                      sx={{
                        width: 15 * answers[index].length,
                        minWidth: 100,
                        "& .MuiInputBase-root": {
                          // "& fieldset": {
                          //   borderColor: "green",
                          // },
                          // "&:hover fieldset": {
                          //   borderColor: "yellow",
                          // },
                          // "&.Mui-focused fieldset": {
                          //   borderColor: "yellow",
                          // },
                          // "&:before": {
                          //   borderBottom: "1px solid white", // Set the desired line color
                          // },
                          // "&:after": {
                          //   borderBottom: "1px solid green", // Set the desired line color when focused
                          // },
                          // "&:hover": {
                          //   borderBottom: " #12202B", // Set the desired line color when focused
                          // },
                        },
                      }}
                      onChange={(e) =>
                        handleUserAnswerChange(e.target.value, index)
                      }
                    />
                    {/* <WhiteBorderTextField
                  
                  ></WhiteBorderTextField> */}
                  </>
                )}
              </div>
            );
          })}
        </div>
      );
    };

    const handleUserAnswerChange = (value: string, index: number) => {
      setUserAnswers((prevAnswers) => {
        const newAnswers = [...prevAnswers];
        newAnswers[index] = value;
        return newAnswers;
      });
    };
    const getImage = (path: string) => {
      if (path != "")
        //flex justify-center
        return (
          <div className="mb-5">
            <img style={{ maxHeight: "200px" }} className="" src={`${path}`} />
          </div>
        );
    };

    return (
      <div className="ps-3">
        {getImage(pathImg)}
        {generateParagraphWithInputs()}
        {/* <Button variant="contained" onClick={handleSubmit}>
          Submit
        </Button> */}
      </div>
    );
  });

export default FillInTheBlankParagraph;
