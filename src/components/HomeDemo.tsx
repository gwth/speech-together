import Image from "next/image";

import DefaultLayout from "@/components/layout/DefaultLayout";
import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardActions from "@mui/material/CardActions";
import Typography from "@mui/material/Typography";
import { useRouter } from "next/router";
import { CardMedia, Divider, Grid, Paper, Stack } from "@mui/material";

const data = [
  // {
  //   text: "4.2.1 ข้อสอบทักษะการฟังภาษาอังกฤษ",
  //   pathImg: "/images/listening.png",
  //   pathGo: "/type/listening",
  //   type: "listening",
  // },
  // {
  //   text: "4.2.2 ข้อสอบทักษะการพูดภาษาอังกฤษ",
  //   pathImg: "./images/speaking.png",
  //   pathGo: "/speaking",
  //   type: "speaking",
  // },
  // {
  //   text: "4.2.3 ข้อสอบทักษะการอ่านภาษาอังกฤษ ",
  //   pathImg: "./images/reading.png",
  //   pathGo: "/type/reading",
  //   type: "reading",
  // },
  // {
  //   text: "4.2.4 ข้อสอบทักษะการเขียนภาษาอังกฤษ (Typing) ",
  //   pathImg: "./images/writing.png",
  //   pathGo: "/fillin",
  //   type: "fillin",
  // },
  {
    text: "ทดสอบการพูด",
    pathImg: "./images/speaking.png",
    pathGo: "/speaking-test-input-passage",
    type: "speaking",
  },
];

export default function HomeDemo() {
  const router = useRouter();
  return (
    <div className="container mx-auto">
      <Grid container spacing={2}>
        {/* <img
          style={{ maxHeight: "300px", maxWidth: "240px" }}
          src={data[0].pathImg}
        ></img> */}
        <Grid item xs={12}>
          <h1 className="text-black mb-2">HOME</h1>
          <Divider sx={{ bgcolor: "rgba(123, 123, 123, 0.4)" }} />
        </Grid>

        <Grid item xs={12}>
          <Grid container justifyContent="center">
            {data.map((item, index: number) => {
              return (
                <Grid key={index} item sx={{ padding: "20px" }}>
                  <Card
                    style={{
                      backgroundColor: "rgba(242, 242, 242, 0.7)",
                    }}
                    sx={{ minWidth: 200, maxWidth: 300 }}
                    className="cursor-pointer transition duration-150 ease-in-out hover:scale-105 rounded-xl"
                    onClick={() => {
                      localStorage.setItem("slug", item.type);
                      router.push(item.pathGo);
                    }}
                  >
                    <CardMedia
                      component="img"
                      height="300"
                      width="240"
                      image={item.pathImg}
                    />

                    <CardContent>
                      <Typography
                        sx={{ fontSize: 20 }}
                        color="text.secondary"
                        gutterBottom
                        className={`text-black font-semibold`}
                      >
                        {item.text}
                      </Typography>
                    </CardContent>
                  </Card>
                </Grid>
              );
            })}
            {/* <Grid item>
            <Card
              sx={{ minWidth: 200, maxWidth: 500 }}
              className="cursor-pointer transition duration-150 ease-in-out hover:scale-105 rounded-xl"
              onClick={() => router.push("/vocabulary3")}
            >
              <CardContent>
                <Typography
                  sx={{ fontSize: 20 }}
                  color="text.secondary"
                  gutterBottom
                >
                  Speaking
                </Typography>
              </CardContent>
            </Card>
          </Grid>
          <Grid item>
            {" "}
            <Card
              sx={{ minWidth: 100, maxWidth: 500 }}
              className="cursor-pointer transition duration-150 ease-in-out hover:scale-105 rounded-xl"
              onClick={() => router.push("/type/listening")}
            >
              <CardContent>
                <Typography
                  sx={{ fontSize: 20 }}
                  color="text.secondary"
                  gutterBottom
                >
                  Listening
                </Typography>
              </CardContent>
            </Card>
          </Grid>
          <Grid item>
            {" "}
            <Card
              sx={{ minWidth: 100, maxWidth: 500 }}
              className="cursor-pointer transition duration-150 ease-in-out hover:scale-105 rounded-xl"
              onClick={() => router.push("/type/reading")}
            >
              <CardContent>
                <Typography
                  sx={{ fontSize: 20 }}
                  color="text.secondary"
                  gutterBottom
                >
                  Reading
                </Typography>
              </CardContent>
            </Card>
          </Grid> */}
          </Grid>
        </Grid>
      </Grid>
    </div>
    // <Grid container>
    //   <Grid item  xs={6}>
    //     <Card sx={{ minWidth: 200, minHeight: 200 }}>1</Card>
    //   </Grid>
    //   <Grid item xs={6}>
    //     <Card sx={{ minWidth: 200, minHeight: 200 }}>1</Card>
    //   </Grid>
    //   <Grid item xs={6}>
    //     <Card sx={{ minWidth: 200, minHeight: 200 }}>1</Card>
    //   </Grid>
    //   <Grid item xs={6}>
    //     <Card sx={{ minWidth: 200, minHeight: 200 }}>1</Card>
    //   </Grid>
    // </Grid>
  );
}
