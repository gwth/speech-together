import React from "react";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import Button from "@mui/material/Button";
import { Typography } from "@mui/material";

interface PopupProps {
  open: boolean;
  onClose: () => void;
  text: string;
}

const PopupDialog: React.FC<PopupProps> = ({ open, onClose, text }) => {
  return (
    <Dialog
      open={open}
      onClose={onClose}
      PaperProps={{ style: { backgroundColor: "rgba(242, 242, 242, 1)" } }}
    >
      <DialogTitle style={{ color: "black" }}>{text}</DialogTitle>

      <DialogContent style={{ textAlign: "right" }}>
        <Button size="small" variant="contained" onClick={onClose}>
          OK
        </Button>
      </DialogContent>
    </Dialog>
  );
};

export default PopupDialog;
