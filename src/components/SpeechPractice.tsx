import React from "react";
import { IconMicrophone, IconVolume } from "@tabler/icons-react";

import CardQuestion from "./speech-practice/CardQuestion";
import CardPlayground from "./speech-practice/CardPlayground";
import CardAssessmentResult from "./speech-practice/CardAssessmentResult";
import CardAction from "./speech-practice/CardAction";
import ButtonIcon from "./speech-practice/ButtonIcon";
import ButtonNext from "./speech-practice/ButtonNext";
import ButtonBack from "./speech-practice/ButtonBack";

import { useAudioRecorder } from "react-audio-voice-recorder";

import { Tooltip } from "react-tooltip";

interface IPronunciationResult {
  scoreNumber: {
    accuracyScore: number;
    fluencyScore: number;
    compScore: number;
    pronScore: number;
  };
  lastWords: Words[];
}

type Words = {
  Word: string;
  PronunciationAssessment: {
    AccuracyScore: number;
    ErrorType: string;
  };
};
function useForceUpdate() {
  let [value, setState] = React.useState(true);
  return () => setState(!value);
}

export default function SpeechPracticeComp({ title, paragraph }: SpeechPracticeProps) {
  const API_ENDPOINT = process.env.NEXT_PUBLIC_API_ENDPOINT;
  const API_PORT = process.env.NEXT_PUBLIC_API_PORT;

  const [stateButton, setStateButton] = React.useState<"next" | "back">("next");

  const [pronunciationResult, setPronunciationResult] = React.useState<IPronunciationResult | null>(null);
  const [words, setWords] = React.useState<Words[]>([]);
  const [recordingStatus, setRecordingStatus] = React.useState<"pre-record" | "post-record">("pre-record");
  const [speechNotDetect, setSpeechNotDetect] = React.useState<boolean>(false);
  const [audioBlob, setAudioBlob] = React.useState<Blob | null>(null);
  const [stateFetch, setStateFetch] = React.useState<"idle" | "loading" | "success" | "error">("idle");

  const audioRef = React.useRef<HTMLAudioElement | null>(null);

  const handleForceUpdate = useForceUpdate();

  const handleStateButtonNext = () => {
    setStateButton("back");
    setAudioBlob(null);
    handleForceUpdate();
    setPronunciationResult(null);
    stopAudio();
    setStateFetch("idle");
  };
  const handleStateButtonBack = () => {
    setAudioBlob(null);
    setStateButton("next");
  };

  const playAudio = () => {
    if (audioBlob) {
      const audioUrl = URL.createObjectURL(audioBlob);
      audioRef.current?.setAttribute("src", audioUrl);
      audioRef.current?.play();
    }
  };

  const stopAudio = () => {
    if (audioRef.current) {
      audioRef.current.pause();
      audioRef.current.currentTime = 0;
    }
  };

  /**
   *  Handle record
   */
  const { startRecording, stopRecording, togglePauseResume, recordingBlob, isRecording, isPaused, recordingTime, mediaRecorder } = useAudioRecorder();

  React.useEffect(() => {
    if (!recordingBlob) return;
    const formData = new FormData();
    formData.append("webmFile", recordingBlob, "input.webm");

    if (recordingStatus === "post-record") {
      setStateFetch("loading");
      formData.append("referent_text", paragraph);

      fetch(`${API_ENDPOINT}:${API_PORT}/api/upload`, {
        method: "POST",
        body: formData,
      })
        .then((response) => {
          if (response.ok) {
            return response.json();
          } else {
            throw new Error("Failed to convert and save.");
          }
        })
        .then((data) => {
          setSpeechNotDetect(data.lastWords[0] === null);
          if (data.lastWords[0] === null) return;
          setPronunciationResult(data);
          setWords(data.lastWords);
          setStateFetch("success");
        })
        .catch((error) => {
          console.error("Error:", error);
        })
        .finally(() => {});
      setAudioBlob(recordingBlob);
    } else {
      setAudioBlob(recordingBlob);
      // fetch("/api/speech-practice", {
      //   method: "POST",
      //   body: formData,
      // })
      //   .then((response) => {
      //     if (response.ok) {
      //       return response.json();
      //     } else {
      //       throw new Error("Failed to convert and save.");
      //     }
      //   })
      //   .then((data) => {})
      //   .catch((error) => {
      //     console.error("Error:", error);
      //   });
    }
  }, [recordingBlob]);

  React.useEffect(() => {
    if (pronunciationResult === null) return;
    if (pronunciationResult.scoreNumber.pronScore < 50) {
      setStateButton("next");
      setAudioBlob(null);
    }
  }, [pronunciationResult]);

  const handleToggleRecord = (status: "pre-record" | "post-record") => {
    setRecordingStatus(status);
    setSpeechNotDetect(false);
    stopAudio();

    if (!isRecording) {
      startRecording();
    } else stopRecording();
  };

  /**
   *  Handle fetch and play audio
   */

  const handlePronunciation = async () => {
    const uri = "http://172.30.16.157:4000/api/pronunciation";
    fetch(uri, { method: "GET" })
      .then(async (response) => await response.json())
      .then((data) => {
        setPronunciationResult(data);
        setWords(data.lastWords);
      });
  };

  const handleColor = (errorType: string) => {
    switch (errorType) {
      case "Omission":
        return "bg-[#72716f]";
      case "Insertion":
        return "bg-[#a80000]";
      case "Mispronunciation":
        return "bg-[#fc0]";
      default:
        return "#000000";
    }
  };

  return (
    <div className="grid grid-cols-2 grid-rows-1 gap-4 mx-3 sm:mx-[5%] xl:mx-[20%]  py-[3rem] h-fit">
      <audio ref={audioRef} className="mb-4" />
      <div className="col-span-2 h-fit">
        <CardQuestion title={title} paragraph={paragraph} />
      </div>
      <div className="col-span-2">
        <div className="grid grid-rows-1 grid-cols-2 gap-4 relative">
          {stateButton === "next" ? (
            <ButtonNext disabled={stateButton === "next" && audioBlob === null} onClick={handleStateButtonNext} />
          ) : (
            <ButtonBack disabled={stateButton === "back" && pronunciationResult === null} onClick={handleStateButtonBack} />
          )}
          <div className="col-span-2 sm:col-span-1 z-0">
            <CardPlayground disabled={stateButton === "back"} title="Let's practice!" icon="/speech-practice/icons/chevron-first.svg">
              <CardAction title={<div className={`text-2xl uppercase font-extrabold text-center text-[#DF715E]`}>Practice your speaking</div>}>
                <ButtonIcon
                  className="bg-[#DF715E] hover:bg-[#d55b46] h-[65px] w-[65px] rounded-2xl border-transparent cursor-pointer"
                  onClick={() => handleToggleRecord("pre-record")}
                  icon={<IconMicrophone size="100%" color="white" />}
                />
                {isRecording && recordingStatus === "pre-record" && <span className="loader"></span>}
              </CardAction>
              <CardAction title={<div className={`text-2xl uppercase font-extrabold text-center text-[#6780C5]`}>Listen to the paragraph</div>}>
                <ButtonIcon
                  className="bg-[#6780C5] hover:bg-[#5472c3] h-[65px] w-[65px] rounded-2xl border-transparent cursor-pointer"
                  onClick={playAudio}
                  icon={<IconVolume size="100%" color="white" />}
                />
              </CardAction>
            </CardPlayground>
          </div>
          <div className="col-span-2 sm:col-span-1 z-0">
            <CardPlayground disabled={stateButton === "next"} title="Let's test!" icon="/speech-practice/icons/chevron-second.svg">
              {stateFetch === "idle" && pronunciationResult === null ? (
                <CardAction title={<div className={`text-2xl uppercase font-extrabold text-center text-[#6780C5]`}>Record your speaking</div>}>
                  <ButtonIcon
                    className="bg-[#6780C5] hover:bg-[#5472c3] h-[65px] w-[65px] rounded-2xl border-transparent cursor-pointer "
                    onClick={() => handleToggleRecord("post-record")}
                    icon={<IconMicrophone size="100%" color="white" />}
                  />

                  {isRecording && recordingStatus === "post-record" && <span className="loader"></span>}
                </CardAction>
              ) : (
                stateFetch === "success" && (
                  <CardAction title={<div className={`text-2xl uppercase font-extrabold text-center text-[#BF4E5B]`}>Assessment Result</div>}>
                    <CardAssessmentResult score={pronunciationResult?.scoreNumber.pronScore || 0} onTryAgain={() => {}} />
                  </CardAction>
                )
              )}

              {stateFetch === "loading" && (
                <div className="flex justify-center items-center h-full w-full">
                  <CardAction title={<div></div>}>
                    <span className="loader-api"></span>
                  </CardAction>
                </div>
              )}

              {speechNotDetect && (
                <CardAction title={<div className={`text-2xl uppercase font-extrabold text-center text-[#BF4E5B]`}>Can Not detect your speech</div>}>
                  <div className="text-2xl uppercase font-extrabold text-center text-[#BF4E5B]">try again please.</div>
                </CardAction>
              )}
            </CardPlayground>
          </div>
        </div>
      </div>
      <div className="col-span-2">
        <div className="grid grid-cols-1 ">
          {pronunciationResult !== null && (
            <div className=" bg-white p-5 px-5 md:px-15 lg:px-20 space-y-4 shadow-[0_8px_30px_rgb(0,0,0,0.12)]">
              <div className="text-2xl uppercase text-[#BF4E5B] font-black">result</div>
              <div className="flex flex-wrap pb-10">
                <Tooltip
                  id="tooltip"
                  render={({ content }) => {
                    return <div>{content}</div>;
                  }}
                />
                {words.map((word, index) => (
                  <div
                    data-tooltip-id="tooltip"
                    data-tooltip-content={`${
                      word.PronunciationAssessment.ErrorType === "None"
                        ? "Accuracy score: " + word.PronunciationAssessment.AccuracyScore
                        : word.PronunciationAssessment.ErrorType
                    }`}
                    className={`inline px-1 m-[1px] cursor-pointer ${handleColor(word.PronunciationAssessment.ErrorType)}`}
                    key={index}>
                    {word.PronunciationAssessment.ErrorType === "Insertion" && (
                      <>
                        <span className="text-xl text-white font-normal line-through">{word.Word}</span>
                      </>
                    )}

                    {word.PronunciationAssessment.ErrorType === "Omission" && (
                      <>
                        <span className="text-xl text-white font-normal ">[ {word.Word} ]</span>
                      </>
                    )}

                    {word.PronunciationAssessment.ErrorType === "Mispronunciation" && (
                      <>
                        <span className="text-xl text-white font-normal">{word.Word}</span>
                      </>
                    )}

                    {word.PronunciationAssessment.ErrorType === "None" && (
                      <>
                        <span className="text-xl font-normal">{word.Word}</span>
                      </>
                    )}
                  </div>
                ))}
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

interface SpeechPracticeProps {
  title: string;
  paragraph: string;
}

export const Paragraphs = [
  {
    id: 5,
    title: "Read the following paragraphs out loud.",
    content:
      "Hello, my name is Suda. I'm from the city. I'm 25 years old. I work in an office. I like reading books and watching movies. I also enjoy going for walks in the park. It’s nice to meet you!",
  },
  {
    id: 2,
    title: "Read the following compound words out loud.",
    content: "Bedroom",
  },
  {
    id: 3,
    title: "Read the following sentences out loud.",
    content: "I like pizza.",
  },
  {
    id: 4,
    title: "Read the following sentences out loud.",
    content: "She has a cat.",
  },
  {
    id: 1,
    title: "Read the following words out loud.",
    content: "Hello",
  },
];
