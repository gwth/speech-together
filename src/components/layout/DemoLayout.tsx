import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";

import { useRouter } from "next/router";
import TemporaryDrawer from "./Drawer";
import { Sarabun } from "next/font/google";
const inter = Sarabun({ subsets: ["latin"], weight: "300", style: ["normal"] });
export default function DefaultLayout({ title, showLogo = false }: { title: string; showLogo?: boolean }) {
  const router = useRouter();

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar
        position="static"
        sx={{
          backgroundColor: "#12202B",
        }}>
        <Toolbar>
          <TemporaryDrawer className="mr-[auto]" />
          <div className="flex justify-space-between items-center space-x-5 mr-[auto]">
            {showLogo && <img style={{ maxHeight: "60px" }} src="../images/Chaiyaphum_Rjbht._Univ.png" className="" loading="lazy" />}
            <Typography className="" sx={{}} variant="h5" component="div">
              {title}
            </Typography>
          </div>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
