import Image from "next/image";
import { Inter } from "next/font/google";
import TextField from "@mui/material/TextField";
import Stack from "@mui/material/Stack";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import Paper from "@mui/material/Paper";
import { styled } from "@mui/material/styles";
import React, { useState, ChangeEvent, useEffect } from "react";
import { typeScore } from "@/hooks/microsoft/types";

type ChildProps = {
  callback2: (value: typeScore) => void;
};

const BoxScore: React.FC<ChildProps> = ({ callback2 }) => {
  const handleClick = () => {
    callback2(inputValue);
  };

  const [inputValue, setInputValue] = useState<typeScore>({
    pronunciation: 19,
    accuracy: 20,
    fluency: 20,
    completeness: 20,
  });

  function btnClick() {
    console.log(inputValue);
  }
  return (
    <Box sx={{ width: 1 }}>
      <h3>Setting Score</h3>
      <Grid container spacing={2}>
        <Grid item xs={9} sm={3} md={3}>
          <h4>Pronunciation score</h4>
        </Grid>
        <Grid item xs={9} sm={9} md={9}>
          <TextField
            id="outlined-basic"
            label="Input score"
            variant="outlined"
            defaultValue={inputValue.pronunciation}
            fullWidth
            InputProps={{ inputProps: { min: 0, max: 100 } }}
            type="number"
            onChange={(e) => {
              setInputValue((o) => ({
                ...o,
                pronunciation: Number(e.target.value),
              }));
              handleClick();
            }}
          />
        </Grid>

        <Grid item xs={9} sm={3} md={3}>
          <h4>Accuracy score</h4>
        </Grid>
        <Grid item xs={9}>
          <TextField
            id="outlined-basic"
            label="Input score"
            variant="outlined"
            type="number"
            fullWidth
            defaultValue={inputValue.accuracy}
            inputProps={{ min: 4, max: 10 }}
            onChange={(e) => {
              setInputValue((o) => ({
                ...o,
                accuracy: Number(e.target.value),
              }));
              handleClick();
            }}
          />
        </Grid>
        <Grid item xs={9} sm={3} md={3}>
          <h4>Fluency score</h4>
        </Grid>
        <Grid item xs={9}>
          <TextField
            id="outlined-basic"
            label="Input score"
            variant="outlined"
            type="number"
            fullWidth
            defaultValue={inputValue.fluency}
            inputProps={{ min: 4, max: 10 }}
            onChange={(e) => {
              setInputValue((o) => ({
                ...o,
                fluency: Number(e.target.value),
              }));
              handleClick();
            }}
          />
        </Grid>
        <Grid item xs={9} sm={3} md={3}>
          <h4>Completeness score</h4>
        </Grid>
        <Grid item xs={9}>
          <TextField
            id="outlined-basic"
            label="Input score"
            variant="outlined"
            type="number"
            fullWidth
            defaultValue={inputValue.completeness}
            inputProps={{ min: 4, max: 10 }}
            onChange={(e) => {
              setInputValue((o) => ({
                ...o,
                completeness: Number(e.target.value),
              }));
              handleClick();
            }}
          />
        </Grid>
        <Grid textAlign="end" item xs={12}>
          <Button onClick={btnClick} variant="contained">
            OK
          </Button>
          <Button onClick={handleClick} variant="contained">
            test
          </Button>
        </Grid>
      </Grid>
    </Box>
  );
};

export default BoxScore;
