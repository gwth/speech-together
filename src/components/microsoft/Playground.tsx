import React, { useEffect } from "react";
import { useRouter } from "next/router";

// mui
import Paper from "@mui/material/Paper";
import Card from "@mui/material/Card";
import IconButton from "@mui/material/IconButton";
import Nossr from "@mui/material/NoSsr";

//css
import styles from "@/styles/conversation.module.css";

// icon
import MicIcon from "@mui/icons-material/Mic";
import VolumeUpIcon from "@mui/icons-material/VolumeUp";

// data mock
import { conversations } from "@/hooks/conversation";
import type { Conversation, Message } from "@/hooks/conversation";

// microsoft
import { useMicrosoft } from "@/hooks/microsoft/v2";
import type { IPronResult } from "@/hooks/microsoft/types";

import Popup from "./Popup";

type Props = {};

export default function Playground({}: Props) {
  const router = useRouter();
  const conversationId = Number(router.query.id);
  const { TextToSpeech, Pronunciation, status } = useMicrosoft();

  // state
  const [messages, setMessages] = React.useState<Message[] | []>([]);
  const [hasConversation, setHasConversation] = React.useState<boolean>(true);
  const [pronResult, setPronResult] = React.useState<IPronResult[]>([]);
  const [currentStep, setCurrentStep] = React.useState<number>(0);
  const [totalStep, setTotalStep] = React.useState<number>(0);
  const [renderText, setRenderText] = React.useState<string>("");
  const [pronScoreCurrent, setPronScoreCurrent] = React.useState<number>(0);
  const [finalScore, setFinalScore] = React.useState<IPronResult>({
    accuracyScore: 0,
    fluencyScore: 0,
    completenessScore: 0,
    pronunciationScore: 0,
  });

  useEffect(() => {
    let current = conversations.find((item) => item.id === conversationId);
    if (current) {
      current.messages = current.messages.sort((a, b) => a.sort - b.sort);
      setTotalStep(current.messages.length);
      setMessages(current.messages);
      setHasConversation(true);
      setCurrentStep(1);
    } else {
      setHasConversation(false);
    }
  }, [conversationId]);

  useEffect(() => {
    let m: Message = messages.filter((item) => item.sort === currentStep)[0];
    handleClickSentence(m);

    if (currentStep > totalStep) {
      handleFinalScore();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentStep]);

  useEffect(() => {
    if (status === "finished") {
      if (pronScoreCurrent)
        if (pronScoreCurrent < 50) setRenderText("Try again");
        else setCurrentStep(currentStep + 1);
      else setCurrentStep(currentStep + 1);
    }
    if (status === "error") setRenderText("Try again");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [status]);

  // useMicrosoft
  const [hiddenLoading, setHiddenLoading] = React.useState<number | null>(null);

  // logic render on page
  const loadingMicAndAudio = (index: number) => {
    if (hiddenLoading === index && status === "processing")
      return <div className={`${styles["dot-flashing"]} self-center mr-10`} />;
    else "";
  };

  // handle click
  const handleClickSentence = (sentence: Message) => {
    setHiddenLoading(sentence?.sort);
    if (sentence?.character === "bot") {
      if (status != "processing") TextToSpeech(sentence?.text);
      setCurrentStep(sentence?.sort);
    }
  };

  const handleClickMic = async (sentence: Message) => {
    setRenderText("");
    if (status != "processing") {
      const pronResultFromFunc = await Pronunciation(sentence.text);
      if (pronResultFromFunc) {
        setPronScoreCurrent(pronResultFromFunc.pronunciationScore);
        if (pronResultFromFunc?.pronunciationScore < 50) {
          setRenderText("Try again");
          return;
        } else {
          setPronResult([
            ...pronResult,
            {
              accuracyScore: pronResultFromFunc.accuracyScore,
              fluencyScore: pronResultFromFunc.fluencyScore,
              pronunciationScore: pronResultFromFunc.pronunciationScore,
              completenessScore: pronResultFromFunc.completenessScore,
            },
          ]);
        }
      } else {
        setRenderText("Try again");
      }
    }
  };

  const handleFinalScore = () => {
    let finalScore = pronResult.reduce(
      (accumulator, currentValue) => {
        return {
          accuracyScore: accumulator.accuracyScore + currentValue.accuracyScore,
          fluencyScore: accumulator.fluencyScore + currentValue.fluencyScore,
          pronunciationScore:
            accumulator.pronunciationScore + currentValue.pronunciationScore,
          completenessScore:
            accumulator.completenessScore + currentValue.completenessScore,
        };
      },
      {
        accuracyScore: 0,
        fluencyScore: 0,
        completenessScore: 0,
        pronunciationScore: 0,
      }
    );

    const count = pronResult.length;

    setFinalScore({
      accuracyScore: Number((finalScore.accuracyScore / count).toFixed(0)),
      fluencyScore: Number((finalScore.fluencyScore / count).toFixed(0)),
      completenessScore: Number(
        (finalScore.completenessScore / count).toFixed(0)
      ),
      pronunciationScore: Number(
        (finalScore.pronunciationScore / count).toFixed(0)
      ),
    });
  };

  // render
  if (!hasConversation)
    return (
      <div className="text-4xl font-bold flex justify-center my-10">
        Conversation Not found
      </div>
    );

  return (
    <>
      <Nossr>
        <div className="text-4xl font-bold flex justify-center my-10">
          Playground {"  "} {status}
        </div>
        {messages.map((item: Message, index: number) => {
          return (
            <Paper
              key={index}
              elevation={currentStep === item.sort ? 16 : 0}
              className={`p-5 m-2 rounded-xl ${
                item.character === "bot" ? "text-gray-500" : "text-blue-500"
              } ${currentStep === item.sort ? "bg-blue-200" : "bg-gray-200"}`}>
              <div className={`flex`}>
                <div className={`text-md font-bold self-center`}>
                  {item.actor}: &nbsp;
                </div>
                <div className="text-md self-center">{item.text}</div>
                <div className="text-md self-center grow text-red-500 font-semibold flex justify-end">
                  {currentStep === item.sort && renderText !== ""
                    ? renderText
                    : ""}
                </div>
                <div className="grow-0 w-[10%]">
                  <div className="flex justify-end">
                    {loadingMicAndAudio(item.sort)}
                    {item.character === "bot"
                      ? currentStep === item.sort && (
                          <IconButton onClick={() => handleClickSentence(item)}>
                            <VolumeUpIcon className="cursor-pointer" />
                          </IconButton>
                        )
                      : currentStep === item.sort && (
                          <IconButton onClick={() => handleClickMic(item)}>
                            <MicIcon className="cursor-pointer" />
                          </IconButton>
                        )}
                  </div>
                </div>
              </div>
            </Paper>
          );
        })}
        <Popup open={currentStep > totalStep} score={finalScore} />
      </Nossr>
    </>
  );
}
