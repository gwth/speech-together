import React, { useEffect } from "react";
import { useRouter } from "next/router";

// mui
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogActions from "@mui/material/DialogActions";
import Button from "@mui/material/Button";

// types
import type { IPronResult } from "@/hooks/microsoft/types";

type Props = {
  open: boolean;
  score: IPronResult;
};

export default function Popup({ open, score }: Props) {
  const router = useRouter();

  const handleClick = () => {
    router.push("/conversations");
  };
  return (
    <>
      <Dialog
        open={open}
        fullWidth
        sx={{
          "MuiPaper-root": {
            borderRadius: "100px",
          },
        }}>
        <DialogTitle
          textAlign="center"
          sx={{
            fontSize: "4rem",
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
            color: "#4a934a",
          }}>
          Successfully
        </DialogTitle>

        <DialogContent
          sx={{
            fontSize: "2rem",
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            paddingTop: "5px",
            paddingBottom: "5px",
          }}>
          <DialogContentText
            sx={{
              fontSize: "1.9rem",
            }}>
            Accuracy:
          </DialogContentText>
          <DialogContentText
            sx={{
              fontSize: "1.9rem",
            }}>
            {score.accuracyScore}
          </DialogContentText>
        </DialogContent>
        <DialogContent
          sx={{
            fontSize: "2rem",
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            paddingTop: "5px",
            paddingBottom: "5px",
          }}>
          <DialogContentText
            sx={{
              fontSize: "1.9rem",
            }}>
            Fluency:
          </DialogContentText>
          <DialogContentText
            sx={{
              fontSize: "1.9rem",
            }}>
            {score.fluencyScore}
          </DialogContentText>
        </DialogContent>
        <DialogContent
          sx={{
            fontSize: "2rem",
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            paddingTop: "5px",
            paddingBottom: "5px",
          }}>
          <DialogContentText
            sx={{
              fontSize: "1.9rem",
            }}>
            Completeness:
          </DialogContentText>
          <DialogContentText
            sx={{
              fontSize: "1.9rem",
            }}>
            {score.completenessScore}
          </DialogContentText>
        </DialogContent>
        <DialogContent
          sx={{
            fontSize: "2rem",
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            paddingTop: "5px",
            paddingBottom: "5px",
          }}>
          <DialogContentText
            sx={{
              fontSize: "1.9rem",
            }}>
            Pronunciation:
          </DialogContentText>
          <DialogContentText
            sx={{
              fontSize: "1.9rem",
            }}>
            {score.pronunciationScore}
          </DialogContentText>
        </DialogContent>

        <DialogActions
          sx={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
          }}>
          <Button
            variant="contained"
            onClick={handleClick}
            sx={{
              borderRadius: "30px",
              width: "100px",
            }}>
            Ok
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
