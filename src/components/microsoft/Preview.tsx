import React, { useEffect } from "react";
import { useRouter } from "next/router";

// mui
import Container from "@mui/material/Container";
import Paper from "@mui/material/Paper";
import Card from "@mui/material/Card";
import Nossr from "@mui/material/NoSsr";

// data mock
import { conversations } from "@/hooks/conversation";
import type { Conversation, Message } from "@/hooks/conversation";

export default function Preview() {
  const router = useRouter();
  const conversationId = Number(router.query.id);

  // state
  const [messages, setMessages] = React.useState<Message[] | []>([]);

  useEffect(() => {
    let current = conversations.find((item) => item.id === conversationId);
    if (current) {
      current.messages = current.messages.sort((a, b) => a.sort - b.sort);
      setMessages(current.messages);
    }
  }, [conversationId]);

  return (
    <>
      <Nossr>
        <div className="text-4xl font-bold flex justify-center my-10">
          Conversation
        </div>
        {messages.map((item: Message, index: number) => {
          return (
            <Paper
              key={index}
              elevation={10}
              className={`p-5 m-2 rounded-xl ${
                item.character === "bot" ? "text-gray-500" : "text-blue-500"
              }`}>
              <div className={`flex`}>
                <div className={`text-md font-bold self-center `}>
                  {item.actor}: &nbsp;
                </div>
                <div className="text-md self-center">{item.text}</div>
              </div>
            </Paper>
          );
        })}
      </Nossr>
    </>
  );
}
