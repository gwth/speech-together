import React, { useEffect, useRef, useState } from "react";

type AudioPlayerProps = Readonly<{
  src: string;
}>;

export default function AudioPlayer({ src }: AudioPlayerProps) {
  const [isPlaying, setIsPlaying] = useState<boolean>(true);
  const [isMuted, setIsMuted] = useState<boolean>(false);
  const [duration, setDuration] = useState<number>(0);
  const [currentTime, setCurrentTime] = useState<number>(0);
  const audioRef = useRef<HTMLAudioElement>(null);
  const timelineRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    const audio = audioRef.current;
    if (!audio) return;

    setDuration(audio.duration);
    /* const handleLoadedMetadata = () => {
      setDuration(audio.duration);
    };

    audio.addEventListener("loadedmetadata", handleLoadedMetadata);

    return () => {
      audio.removeEventListener("loadedmetadata", handleLoadedMetadata);
    }; */
  }, [src]);

  const toggleAudio = (): void => {
    const audio = audioRef.current;
    if (audio?.paused) {
      audio.play();
    } else {
      audio?.pause();
    }
    setIsPlaying(audio?.paused ?? false);
  };

  const changeTimelinePosition = (): void => {
    const audio = audioRef.current;
    if (!audio) return;
    const percentagePosition = (100 * audio.currentTime) / audio.duration;
    console.log("🚀 ~ changeTimelinePosition ~ percentagePosition:", audio.duration, audio.currentTime);

    timelineRef.current!.style.backgroundSize = `${percentagePosition}% 100%`;

    setCurrentTime(audio.currentTime);
  };

  const audioEnded = (): void => {
    setIsPlaying(true);
  };

  const changeSeek = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const audio = audioRef.current;
    if (!audio) return;
    const time = (parseFloat(event.target.value) * audio.duration) / 100;
    audio.currentTime = time;
    setCurrentTime(time);
  };

  const formatTime = (time: number): string => {
    const minutes = Math.floor(time / 60);
    const seconds = Math.floor(time % 60);
    return `${minutes}:${seconds < 10 ? "0" : ""}${seconds}`;
  };

  const toggleSound = (): void => {
    const audio = audioRef.current;
    if (!audio) return;
    audio.muted = !audio.muted;
    setIsMuted(audio.muted);
  };

  return (
    <div className="audio-player">
      <audio ref={audioRef} src={src} onTimeUpdate={changeTimelinePosition} onEnded={audioEnded} />
      <div className="controls">
        <button className="player-button" onClick={toggleAudio}>
          {isPlaying ? (
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="#57BCE2">
              <path
                fillRule="evenodd"
                d="M10 18a8 8 0 100-16 8 8 0 000 16zM9.555 7.168A1 1 0 008 8v4a1 1 0 001.555.832l3-2a1 1 0 000-1.664l-3-2z"
                clipRule="evenodd"
              />
            </svg>
          ) : (
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="#57BCE2">
              <path
                fillRule="evenodd"
                d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zM7 8a1 1 0 012 0v4a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v4a1 1 0 102 0V8a1 1 0 00-1-1z"
                clipRule="evenodd"
              />
            </svg>
          )}
        </button>
        <div className="mx-2">
          <span>{formatTime(currentTime)}</span>
        </div>

        <input type="range" className="timeline" ref={timelineRef} max="100" value={(currentTime / (audioRef.current?.duration ?? 1)) * 100} />
        <div>
          {/* <span>{formatTime(currentTime)}</span> */}
          {/* <span>{formatTime(currentTime)}</span> / <span>{formatTime(duration)}</span>1 */}
        </div>
        {/* <button className="sound-button" onClick={toggleSound}>
          {isMuted ? (
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="#3D3132">
              <path
                fillRule="evenodd"
                d="M9.383 3.076A1 1 0 0110 4v12a1 1 0 01-1.707.707L4.586 13H2a1 1 0 01-1-1V8a1 1 0 011-1h2.586l3.707-3.707a1 1 0 011.09-.217zM14.657 2.929a1 1 0 011.414 0A9.972 9.972 0 0119 10a9.972 9.972 0 01-2.929 7.071 1 1 0 01-1.414-1.414A7.971 7.971 0 0017 10c0-2.21-.894-4.208-2.343-5.657a1 1 0 010-1.414zm-2.829 2.828a1 1 0 011.415 0A5.983 5.983 0 0115 10a5.984 5.984 0 01-1.757 4.243 1 1 0 01-1.415-1.415A3.984 3.984 0 0013 10a3.983 3.983 0 00-1.172-2.828 1 1 0 010-1.415z"
                clipRule="evenodd"
              />
            </svg>
          ) : (
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="#3D3132">
              <path
                fillRule="evenodd"
                d="M9.383 3.076A1 1 0 0110 4v12a1 1 0 01-1.707.707L4.586 13H2a1 1 0 01-1-1V8a1 1 0 011-1h2.586l3.707-3.707a1 1 0 011.09-.217zM12.293 7.293a1 1 0 011.414 0L15 8.586l1.293-1.293a1 1 0 111.414 1.414L16.414 10l1.293 1.293a1 1 0 01-1.414 1.414L15 11.414l-1.293 1.293a1 1 0 01-1.414-1.414L13.586 10l-1.293-1.293a1 1 0 010-1.414z"
                clipRule="evenodd"
              />
            </svg>
          )}
        </button> */}
      </div>
    </div>
  );
}
