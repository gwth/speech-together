type ButtonProps = Readonly<{
  onClick?: () => void;
  iconProps?: React.ReactNode;
  label: string;
  isDisabled?: boolean;
}>;

export default function Button({
  onClick,
  iconProps,
  label,
  isDisabled,
}: ButtonProps) {
  return (
    <button
      onClick={onClick}
      disabled={isDisabled}
      type="button"
      className={`${
        isDisabled ? "bg-gray-400" : "bg-[#0077FF] hover:bg-[#3994FE]"
      } custom-box-shadow border-0 rounded-lg h-12 w-fit p-1 px-3 flex flex-row justify-center items-center gap-1 cursor-pointer`}
    >
      <div className="text-white text-lg font-bold">{label}</div>
      {iconProps ? <>{iconProps}</> : null}
    </button>
  );
}
