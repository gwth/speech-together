type HeaderProps = Readonly<{
  label: string;
}>;

export default function Header({ label }: HeaderProps) {
  return (
    <div className="flex flex-row justify-start items-center gap-3">
      <div className="border-4  border-indigo-500 border-solid rounded-lg h-[45px]"></div>
      <div className="text-4xl font-bold">{label}</div>
    </div>
  );
}
