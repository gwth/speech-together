export default function Header() {
  return (
    <div className="bg-[#46997F] h-[70px] w-full flex flex-row justify-center items-center">
      <div className="flex flex-row justify-space-between w-[60%]">
        <div className="inline font-bold text-xl">Speaking Test</div>
      </div>
    </div>
  );
}
