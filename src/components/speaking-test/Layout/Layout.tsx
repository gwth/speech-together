import Header from "./Header";

type LayoutProps = Readonly<{ children: React.ReactNode }>;

export default function Layout({ children }: LayoutProps) {
  return (
    <div>
      <Header />
      <div className="">{children}</div>
    </div>
  );
}
