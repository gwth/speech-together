import { IconVolume, IconPlayerStop } from "@tabler/icons-react";
import React, { useState } from "react";

type RecordButtonProps = Readonly<{
  onClick?: () => void;
  icon?: React.ReactNode;
  time?: number;
}>;

export default function Listen({
  icon,
  onClick,
  time = 5000,
}: RecordButtonProps) {
  const [isPlaying, setIsPlaying] = useState(false);
  const togglePlay = () => {
    console.log("toggle ", isPlaying);
    if (isPlaying) {
    } else {
      setIsPlaying(true);
      setTimeout(function () {
        setIsPlaying(false);
      }, time);
      onClick?.();
    }
  };
  return (
    <button
      onClick={() => {
        togglePlay();
      }}
      type="button"
      className="bg-[#57BCE2]  border-0 rounded-lg h-9  w-9 p-1    flex flex-row justify-center items-center gap-1 cursor-pointer hover:bg-[#A6DEF2] custom-box-shadow"
    >
      <IconVolume color="white" size={20} />
    </button>
  );
}
