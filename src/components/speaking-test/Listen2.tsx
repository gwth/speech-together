import { IconPlayerPause, IconVolume } from "@tabler/icons-react";
import React, { useState, useRef } from "react";

const Listen2 = ({ audioUrl }: { audioUrl: string }) => {
  const [isPlaying, setIsPlaying] = useState(false);
  const audioRef = useRef<HTMLAudioElement>(null);

  const togglePlay = () => {
    if (!audioRef.current) return;

    if (isPlaying) {
      audioRef.current.pause();
    } else {
      audioRef.current.play();
    }

    setIsPlaying(!isPlaying);
  };

  return (
    <div>
      <audio ref={audioRef} src={audioUrl} onEnded={() => setIsPlaying(false)} />
      {/* <button onClick={togglePlay}>{isPlaying ? "Pause" : "Play"}</button> */}
      {isPlaying ? (
        <button
          type="button"
          onClick={togglePlay}
          className="bg-red-500  border-0 rounded-lg h-9  w-9 p-1    flex flex-row justify-center items-center gap-1 cursor-pointer hover:bg-red-400 custom-box-shadow">
          <IconPlayerPause color="white" size={20} />
        </button>
      ) : (
        <button
          type="button"
          onClick={togglePlay}
          className="bg-[#57BCE2]  border-0 rounded-lg h-9  w-9 p-1    flex flex-row justify-center items-center gap-1 cursor-pointer hover:bg-[#A6DEF2] custom-box-shadow">
          <IconVolume color="white" size={20} />
        </button>
      )}
    </div>
  );
};

export default Listen2;
