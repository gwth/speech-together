import React from "react";

export default function Loading() {
  return (
    <div className="custom-box-shadow h-5 rounded-xl bg-white p-4 absolute top-25 left-10 right-10 flex flex-row justify-center items-center gap-5">
      <div className="dot-spinner">
        <div className="dot-spinner__dot" />
        <div className="dot-spinner__dot" />
        <div className="dot-spinner__dot" />
        <div className="dot-spinner__dot" />
        <div className="dot-spinner__dot" />
        <div className="dot-spinner__dot" />
        <div className="dot-spinner__dot" />
        <div className="dot-spinner__dot" />
      </div>

      <div className="text-black text-xl ">Processing</div>
    </div>
  );
}
