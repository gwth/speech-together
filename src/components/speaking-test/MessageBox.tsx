import Listen from "./Listen";
import Listen2 from "./Listen2";

type MessageBox = Readonly<{
  passage: string;

  onListen: (passage: string) => void;
}>;

export default function MessageBox({ passage, onListen }: MessageBox) {
  return (
    <div className="rounded-2xl px-8 p-6 bg-white min-h-[120px] custom-box-shadow  ">
      <div className="flex flex-row justify-between items-start gap-4 ">
        <div>
          <div className="flex flex-row justify-start gap-5 items-center ">
            <div className="text-xl font-bold text-black normal-case ">
              Read the passage
            </div>
            <Listen onClick={() => onListen(passage)} time={3000} />
            {/* <Listen2 audioUrl="/sounds/Eng_Demo.mp3" /> */}
          </div>
          <div className="border w-full border-[#B9B9B9] border-solid my-3 "></div>
          <div className="text-xl font-normal">{passage}</div>
        </div>

        {/*   <div className="bg-[#FFF1A8]  h-[100px] min-w-[120px] rounded-xl flex flex-col items-center justify-start p-3 aspect-square">
          <IconVolume size={90} color="#D49F0E" />
          <div className="text-[#D49F0E] font-extrabold text-lg"> Listen</div>
        </div> */}
      </div>
    </div>
  );
}
