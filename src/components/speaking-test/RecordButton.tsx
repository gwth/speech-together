import { IconMicrophone } from "@tabler/icons-react";
import React from "react";

type RecordButtonProps = Readonly<{
  onClick?: () => void;
  icon?: React.ReactNode;
  className?: string;
  isRecording: boolean;
}>;

export default function RecordButton({ onClick, icon, className, isRecording }: RecordButtonProps) {
  return (
    <div className="flex flex-row justify-start items-center gap-4">
      {isRecording ? (
        <>
          <button
            onClick={onClick}
            type="button"
            className="bg-red-500  border-0 rounded-lg h-12 w-12  p-1 px-3  flex flex-row justify-center items-center gap-1 cursor-pointer hover:bg-red-400 custom-box-shadow">
            <div className="bg-white h-5 w-5"></div>
          </button>
          <div className="flex flex-row justify-start items-center">Recording...</div>
        </>
      ) : (
        <button
          onClick={onClick}
          type="button"
          className={`bg-[#57BCE2]   border-0 rounded-lg h-12  w-12 p-1 px-3  flex flex-row justify-center items-center gap-1 cursor-pointer hover:bg-[#A6DEF2] custom-box-shadow ${className}`}>
          <IconMicrophone color="white" />
        </button>
      )}
    </div>
  );
}
