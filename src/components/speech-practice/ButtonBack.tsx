const ButtonBack = ({ onClick, disabled }: ButtonBackProps) => (
  <button
    type="button"
    onClick={onClick}
    disabled={disabled}
    className={`bg-[#FBBA5A] hover:bg-[#f3ae47]  shadow-[0_8px_30px_rgb(0,0,0,0.12)] cursor-pointer origin-center rotate-90 sm:rotate-0 space-y-4 absolute z-10 h-[120px] w-[40px] sm:w-[70px] lg:h-[150px]  lg:w-[70px] top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 rounded-3xl border-transparent ${
      disabled ? "opacity-50 pointer-events-none" : "" // Apply styles for disabled state
    } `}>
    <img src="/speech-practice/icons/next.svg" className="h-[20px] w-[20px] sm:h-[45px] sm:w-[45px] transform -scale-x-100" alt="next-icon" />
    <div className="uppercase text-white font-extrabold text-md md:text-lg font-sarabun -rotate-90 sm:rotate-0">back</div>
  </button>
);

interface ButtonBackProps {
  onClick?: () => void;
  disabled?: boolean;
}
export default ButtonBack;
