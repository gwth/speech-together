export default function ButtonIcon({ onClick, icon, ...props }: ButtonIconProps) {
  return (
    <button {...props} type="button" onClick={onClick}>
      {icon}
    </button>
  );
}
interface ButtonIconProps {
  onClick?: () => void;
  className?: string;
  icon?: React.ReactNode;
}
