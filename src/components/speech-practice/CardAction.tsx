export default function CardAction({ title, children }: CardActionProps) {
  return (
    <div className="flex flex-col items-center space-y-4  py-10">
      {title}
      {children}
    </div>
  );
}
interface CardActionProps {
  title: React.ReactNode;
  children: React.ReactNode;
}
