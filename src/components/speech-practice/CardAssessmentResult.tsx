import Progress from "./Progress";
import { IconReload } from "@tabler/icons-react";

export default function CardAssessmentResult({ score, onTryAgain }: CardAssessmentResultProps) {
  return (
    <div className="flex flex-col space-y-4 items-center">
      <Progress variant="determinate" size={150} thickness={3} value={score} />
      <div className="text-normal text-xl uppercase text-[#444444]">pronunciation score</div>
      {/* <button
        type="button"
        onClick={onTryAgain}
        className="flex flex-row items-center align-center cursor-pointer space-x-2 w-fit px-3 py-2 bg-red-500 hover:bg-red-600 border-transparent rounded-2xl">
        <IconReload size="35px" stroke={3} color="white" />
        <div className="uppercase font-black text-lg text-white font-sarabun">try again!</div>
      </button> */}
    </div>
  );
}

interface CardAssessmentResultProps {
  score: number;
  onTryAgain: () => void;
}
