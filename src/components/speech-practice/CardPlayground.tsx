import React from "react";

export default function CardPlayground({ title, children, icon, disabled }: CardPlaygroundProps) {
  return (
    <div className={`grid grid-cols-1 grid-rows-1  shadow-[0_8px_30px_rgb(0,0,0,0.12)] ${disabled ? "opacity-50 pointer-events-none" : ""}`}>
      <div className="col-span-1 h-fit">
        <div className="flex justify-start items-center px-5 gap-5 bg-[#B6A5E1] h-12">
          <img src={icon} className="h-[60%]" />
          <div className="text-white uppercase font-bold text-xl">{title}</div>
        </div>
        <div className="bg-white h-[400px]">{children}</div>
      </div>
    </div>
  );
}

interface CardPlaygroundProps {
  title?: string;
  children?: React.ReactNode;
  icon?: string;
  disabled?: boolean; // Add a 'disabled' prop to enable or disable the card
}
