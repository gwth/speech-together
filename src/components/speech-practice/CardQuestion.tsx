import React from "react";

import { IconHelpSquareRounded } from "@tabler/icons-react";

export default function CardQuestion({ title, paragraph }: CardQuestionProps) {
  return (
    <div className="grid grid-cols-1 ">
      <div className="flex items-center align-center space-x-2 bg-[#BF4E5B]   w-fit rounded-t-2xl p-1 px-4">
        <IconHelpSquareRounded
          color="#BF4E5B"
          style={{
            fill: "#FCD0C5",
          }}
          size={25}
        />
        <div className="uppercase text-white">question</div>
      </div>
      <div className=" bg-white p-5 px-5 md:px-15 lg:px-20 space-y-4 shadow-[0_8px_30px_rgb(0,0,0,0.12)]">
        <div className="text-2xl text-[#BF4E5B] font-black">{title}</div>
        {typeof paragraph === "string" ? <div className="text-xl pb-5 break-words">{paragraph}</div> : paragraph}
      </div>
    </div>
  );
}

interface CardQuestionProps {
  title: string;
  paragraph: string | React.ReactNode;
}
