interface Conversation {
  id: number;
  name: string;
  description?: string;
  messages: Message[];
  sort: number;
}

interface Message {
  id: number;
  text: string;
  actor: string;
  character: string;
  sort: number;
}

const messages1: Message[] = [
  {
    id: 1,
    actor: "Katherine",
    character: "bot",
    text: ": Good morning! I’m Katherine Parker. I’m here to see Mrs. Thanwaree.",
    sort: 1,
  },
  {
    id: 2,
    actor: "Sukanya",
    character: "human",
    text: "Certainly. You must be Katherine. I’m Sukanya, the principal secretary.",
    sort: 2,
  },
  {
    id: 3,
    actor: "Katherine",
    character: "bot",
    text: "Oh, hi! I remember your name. I saw it in the email. It’s nice to finally meet you in person.",
    sort: 3,
  },
  {
    id: 4,
    actor: "Sukanya",
    character: "human",
    text: "It’s lovely to meet you, too. Please have a seat. The principal will be ready to see you, soon.",
    sort: 4,
  },
  {
    id: 5,
    actor: "Katherine",
    character: "bot",
    text: "Thank you.",
    sort: 5,
  },
];

const messages2: Message[] = [
  {
    id: 1,
    actor: "Mike",
    character: "human",
    text: " I’d like a room for two people, for three nights please.",
    sort: 1,
  },
  {
    id: 2,
    actor: " Receptionist",
    character: "bot",
    text: "Ok, I just need you to fill in this form please.",
    sort: 2,
  },
  {
    id: 3,
    actor: " Receptionist",
    character: "bot",
    text: "Do you want breakfast?",
    sort: 3,
  },
  {
    id: 4,
    actor: "Mike",
    character: "human",
    text: "Yes, please.",
    sort: 4,
  },
  {
    id: 5,
    actor: " Receptionist",
    character: "bot",
    text: "Breakfast is from 7 to 10 each morning in the dining room. Here is your key. Your room number is 345, on the third floor. Enjoy your stay.",
    sort: 5,
  },
  {
    id: 6,
    actor: "Mike",
    character: "human",
    text: "Thank you.",
    sort: 6,
  },
];

const conversations: Conversation[] = [
  {
    id: 1,
    name: "What’s Your Name?",
    description: "This is a conversation",
    messages: messages1,
    sort: 1,
  },
  {
    id: 2,
    name: "Arriving at the Hotel",
    description: "This is another conversation",
    messages: messages2,
    sort: 2,
  },
];

export type { Conversation, Message };
export { conversations };
