import * as sdk from "microsoft-cognitiveservices-speech-sdk";

function useStateCustom<T>(initial: T): [T, (curretnValue: T) => void] {
  let value = initial;
  function setValue(curretnValue: T) {
    value = curretnValue;
  }

  return [value, setValue];
}

type TStatus =
  | "starting"
  | "processing"
  | "stoped"
  | "canceled"
  | "error"
  | "idle";

interface IPronunciationResult {
  status: TStatus;
  text: string | null;
  error?: string | null;
}

interface IReturnPronunciation {
  startMic: () => void;
  stopMic: () => string;
}

function newPronunciationResult(
  status: TStatus,
  text: string | null,
  error?: string | null
): IPronunciationResult {
  return {
    status,
    text,
    error,
  };
}

export default class Speech {
  private key: string = process.env.MICROSOFT_KEY || "";
  private region: string = process.env.MICROSOFT_REGION || "";
  private speechConfig = sdk.SpeechConfig.fromSubscription(this.key, this.region);
  private audioConfig = sdk.AudioConfig.fromDefaultMicrophoneInput();

  private recognizer: sdk.SpeechRecognizer | null;
  private microphoneOpen: boolean;
  private pronunciationAssessmentConfig: sdk.PronunciationAssessmentConfig | null;
  private pronunciationResult: sdk.PronunciationAssessmentResult | null;

  constructor() {
    this.microphoneOpen = false;
    this.recognizer = null;
    this.pronunciationAssessmentConfig = null;
    this.pronunciationResult = null;
  }

  public pronunciationConfig(text: string) {
    const options = {
      referenceText: text,
      gradingSystem: "HundredMark",
      granularity: "Phoneme",
      phonemeAlphabet: "IPA",
    }

    this.pronunciationAssessmentConfig = sdk.PronunciationAssessmentConfig.fromJSON(JSON.stringify(options));
    this.speechConfig.speechRecognitionLanguage = "en-US";
    this.initialRecognizer()
  }

  private initialRecognizer() {
    this.recognizer = new sdk.SpeechRecognizer(
      this.speechConfig,
      this.audioConfig
    );

    this.pronunciationAssessmentConfig?.applyTo(this.recognizer)
  }

  public toggleMic(): boolean {
    if (this.microphoneOpen) {
      this.microphoneOpen = false;

      if (this.recognizer) {
        this.recognizer.stopContinuousRecognitionAsync();
        this.recognizer = null;
      }
    } else {
      this.microphoneOpen = true;
      if (!this.recognizer)
        this.initialRecognizer()

      this.recognizer?.startContinuousRecognitionAsync();

      if (this.recognizer)
        this.recognizer.recognized = (s, e) => {
          if (e.result.reason == sdk.ResultReason.RecognizedSpeech) {
            console.log(`RECOGNIZED: Text=${e.result.text}`);
            this.pronunciationResult =
              sdk.PronunciationAssessmentResult.fromResult(e.result);
            console.log('result', this.pronunciationResult)
          } else {
            console.log('error')
          }
        };
    }

    return this.microphoneOpen;
  }

  recognize(): Promise<sdk.PronunciationAssessmentResult> {
    return new Promise((resolve, reject) => {
      if (!this.recognizer) {
        reject(new Error("Speech recognition not initial").message);
        return;
      }

      this.recognizer.recognized = (s, e) => {
        if (e.result.reason == sdk.ResultReason.RecognizedSpeech) {
          console.log(`RECOGNIZED: Text=${e.result.text}`);
          this.pronunciationResult =
            sdk.PronunciationAssessmentResult.fromResult(e.result);
          resolve(this.pronunciationResult);
        }
      };

      this.recognizer.canceled = (s, e) => {
        console.log(`CANCELED: Reason=${e.reason}`);

        reject(new Error(e.reason.toString()).message);
      };
    });
  }
}
