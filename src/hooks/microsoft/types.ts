import * as sdk from "microsoft-cognitiveservices-speech-sdk";
export interface IPronResult {
  accuracyScore: number;
  fluencyScore: number;
  completenessScore: number;
  pronunciationScore: number;

}

export interface IPronResultVocab {
  accuracyScore: number;
  fluencyScore: number;
  completenessScore: number;
  pronunciationScore: number;
  index:number;
  // detailResult:sdk.PronunciationAssessmentResult;
  detailResult:any;

}

 export type typeScore = {
    pronunciation: number;
    accuracy: number;
    fluency: number;
    completeness: number;
  };


