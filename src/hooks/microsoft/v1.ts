import * as sdk from "microsoft-cognitiveservices-speech-sdk";

const key: string = process.env.MICROSOFT_KEY || "";
const region: string = process.env.MICROSOFT_REGION || "";
var player: sdk.SpeakerAudioDestination | undefined =
  new sdk.SpeakerAudioDestination();
const speechConfig = sdk.SpeechConfig.fromSubscription(key, region);

function StopPlayer() {
  if (player?.isClosed == true) {
    console.log("player closed");
    player.mute();
    player.close();
    player = new sdk.SpeakerAudioDestination();
  }
}
let isPlaying = false;
let idPlayer = "";
let soundStop = false;
function TextToSpeech(
  text: string,
  notUsePlayer: boolean = false,
  isStop: boolean = false
) {
  try {
    console.log("player ", player?.isClosed);
    if (player?.isClosed == true && !notUsePlayer) {
      console.log("player closed");
      player.mute();
      player.close();
      player = new sdk.SpeakerAudioDestination();
      if (isStop && isPlaying) {
        isPlaying = false;
        soundStop = true;
        return;
      }
    }
    if (!player && !notUsePlayer) {
      return;
    }
    speechConfig.speechSynthesisVoiceName = "en-US-JennyNeural";
    const audioConfig = sdk.AudioConfig.fromSpeakerOutput(
      notUsePlayer ? undefined : player
    );
    const synthesizer = new sdk.SpeechSynthesizer(speechConfig, audioConfig);
    // if (player ) {
    //   player.close();
    // }
    isPlaying = true;
    if (player) {
      player.onAudioEnd = () => {
        console.log("Player has ended sound. ", player?.id());
        if (!soundStop) {
          isPlaying = false;
        }
      };
    }

    // console.log(player?.isClosed);
    synthesizer.speakTextAsync(
      text,
      (result) => {
        console.log("result", result);
        if (result.reason === sdk.ResultReason.SynthesizingAudioCompleted) {
          console.log("synthesis finished.");
        } else {
          console.error(
            "Speech synthesis canceled, " +
              result.errorDetails +
              "\nDid you set the speech resource key and region values?"
          );
        }
        synthesizer.close();
        // synthesizer.dispose();
      },
      (error) => {
        console.trace("err - " + error);
        synthesizer.close();
        // synthesizer.dispose();
      }
    );
  } catch (err) {
    console.log("error", err);
  }
}

function SpeechToText() {
  speechConfig.speechRecognitionLanguage = "en-US";
  const audioConfig = sdk.AudioConfig.fromDefaultMicrophoneInput();
  const recognizer = new sdk.SpeechRecognizer(speechConfig, audioConfig);

  recognizer.recognizeOnceAsync(
    (result) => {
      console.log(`RECOGNIZED: Text=${result.text}`);
      // text = result.text;
      recognizer.close();
      // recognizer.dispose();
    },
    (err) => {
      console.trace("err - " + err);
      recognizer.close();
      // recognizer.dispose();
    }
  );
}

function SpeechToTextPronunciation(text: string) {
  var pronunciationAssessmentConfig =
    sdk.PronunciationAssessmentConfig.fromJSON(
      `{"referenceText":"${text}","gradingSystem":"HundredMark","granularity":"Phoneme","phonemeAlphabet":"IPA","nBestPhonemeCount":5}`
    );

  speechConfig.speechRecognitionLanguage = "en-US";
  const audioConfig = sdk.AudioConfig.fromDefaultMicrophoneInput();
  const recognizer = new sdk.SpeechRecognizer(speechConfig, audioConfig);

  pronunciationAssessmentConfig.applyTo(recognizer);

  recognizer.recognizeOnceAsync(
    (result) => {
      console.log(`RECOGNIZED: Text=${result.text}`);
      // The pronunciation assessment result as a Speech SDK object
      const pronunciationAssessmentResult =
        sdk.PronunciationAssessmentResult.fromResult(result);
      console.log(
        "pronunciationAssessmentResult Object:",
        pronunciationAssessmentResult
      );

      // The pronunciation assessment result as a JSON string
      var pronunciationAssessmentResultJson = result.properties.getProperty(
        sdk.PropertyId.SpeechServiceResponse_JsonResult
      );

      console.log(
        "pronunciationAssessmentResultJsonSting",
        pronunciationAssessmentResultJson
      );

      recognizer.close();
      // recognizer.dispose();
    },
    (err) => {
      console.trace("err - " + err);
      recognizer.close();
      // recognizer.dispose();
    }
  );
}

export const useMicrosoft = function () {
  return {
    TextToSpeech,
    SpeechToText,
    SpeechToTextPronunciation,
    StopPlayer,
  };
};

// export { useTextToSpeech, useSpeechToText, useSpeechToTextPronunciation };
