import * as sdk from "microsoft-cognitiveservices-speech-sdk";


import { useState } from "react";

const key: string = process.env.MICROSOFT_KEY || "";
const region: string = process.env.MICROSOFT_REGION || "";

const speechConfig = sdk.SpeechConfig.fromSubscription(key, region);
speechConfig.speechSynthesisVoiceName = "en-US-AriaNeural";
speechConfig.speechRecognitionLanguage = "en-US";

async function getAudioDuration(arrayBuffer: ArrayBuffer): Promise<number> {
  const audioContext = new AudioContext();
  const audioBuffer = await audioContext.decodeAudioData(arrayBuffer);
  const duration = audioBuffer.duration;
  return duration;
}

async function speakingSync(duration: number): Promise<void> {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, duration);
  });
}

function waitUntil(condition: () => boolean): Promise<void> {
  return new Promise<void>((resolve) => {
    const checkCondition = async () => {
      while (!condition()) {
        await new Promise((innerResolve) => setTimeout(innerResolve, 1000)); // Wait for 1 second
      }
      resolve();
    };

    checkCondition();
  });
}

export function useMicrosoft() {
  // Text to speech
  type TStatus =
    | "idle"
    | "speaking"
    | "listening"
    | "processing"
    | "error"
    | "finished";

  const [status, setStatus] = useState<TStatus>("idle");

  async function TextToSpeech(text: string) {
    const synthesizer: sdk.SpeechSynthesizer = new sdk.SpeechSynthesizer(
      speechConfig
    );

    setStatus("processing");
    synthesizer.speakTextAsync(
      text,
      async (result) => {
        if (result.reason === sdk.ResultReason.SynthesizingAudioCompleted) {
          const { audioData } = result;
          const duration = await getAudioDuration(audioData);
          const durationMilliseconds = duration * 1000;
          console.log(
            `Audio duration is ${duration} seconds. milliseconds: ${durationMilliseconds}`
          );

          await speakingSync(durationMilliseconds);
        } else {
          console.error("Speech synthesis canceled, " + result.errorDetails);
        }
        setStatus("finished");

        console.log("closed");
        synthesizer.close();
      },
      (error) => {
        console.trace("err - " + error);
        setStatus("error");
        synthesizer.close();
      }
    );
  }

  // Pronunciation
  type TPronunciationResult = sdk.PronunciationAssessmentResult | null;
  async function Pronunciation(text: string): Promise<TPronunciationResult> {
    var pronunciationAssessmentConfig =
      sdk.PronunciationAssessmentConfig.fromJSON(
        `{"referenceText":"${text}","gradingSystem":"HundredMark","granularity":"Phoneme","phonemeAlphabet":"IPA","nBestPhonemeCount":5,"EnableMiscue":true}`
      );
     // sdk.PronunciationAssessmentConfig.fromJSON("{\"referenceText\":\"good morning\",\"gradingSystem\":\"HundredMark\",\"granularity\":\"Phoneme\",\"EnableMiscue\":true}");
    console.log('pronunciationAssessmentConfig',pronunciationAssessmentConfig)
    const audioConfig = sdk.AudioConfig.fromDefaultMicrophoneInput();
    
    const recognizer = new sdk.SpeechRecognizer(speechConfig, audioConfig);
   // console.log('speechConfig',speechConfig)

    pronunciationAssessmentConfig.applyTo(recognizer);
    //console.log('recognizer',recognizer)

    let pronunciationResult: TPronunciationResult = null;
    
    let timeoutDuration =5000; // 5 seconds
    switch (true) {
  case text.length < 50:
    timeoutDuration = 3000
    break;
  case text.length < 100:
    timeoutDuration = 7000
    break;
  case text.length < 200:
   timeoutDuration = 20000
    break;
    case text.length > 300:
   timeoutDuration = 40000
    break;
  default:
     timeoutDuration = 5000
    break;
}
  const timeout = setTimeout(() => {
  
    if(recognizer){
     // recognizer.stopContinuousRecognitionAsync();
     recognizer.stopContinuousRecognitionAsync();
      console.log('No sound detected within the specified duration.');
    }
    
  }, timeoutDuration);

    setStatus("processing");
    //console.log("sssssssssssssssssssssssssssssssssssss")
    recognizer.recognizeOnceAsync(
      (result) => {
        switch (result.reason) {
          case sdk.ResultReason.RecognizedSpeech:
            console.log(`RECOGNIZED: Text=${result.text}`);
            pronunciationResult =
              sdk.PronunciationAssessmentResult.fromResult(result);
            console.log("pronunciationResult", pronunciationResult);
            break;
          case sdk.ResultReason.NoMatch:
            console.log("NOMATCH: Speech could not be recognized.");
            setStatus("error");
            break;
          case sdk.ResultReason.Canceled:
            const cancellation = sdk.CancellationDetails.fromResult(result);
            console.log(`CANCELED: Reason=${cancellation.reason}`);

            if (cancellation.reason == sdk.CancellationReason.Error) {
              setStatus("error");
              console.log(`CANCELED: ErrorCode=${cancellation.ErrorCode}`);
              console.log(
                `CANCELED: ErrorDetails=${cancellation.errorDetails}`
              );
              console.log(
                "CANCELED: Did you set the speech resource key and region values?"
              );
            }
            break;
        }

        recognizer.close();
      },
      (err) => {
        console.trace("err - " + err);
        setStatus("error");
        recognizer.close();
      }
    );

    await waitUntil(() => pronunciationResult !== null);
    if (pronunciationResult) {
      setStatus("finished");
    } else {
      setStatus("error");
    }

    return pronunciationResult;
  }
 async function blobToArrayBuffer(blob:Blob): Promise<ArrayBuffer> {
    if ('arrayBuffer' in blob) return await blob.arrayBuffer();

    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onload = () => resolve(reader.result as ArrayBuffer) ;
        reader.onerror = () => reject();
        reader.readAsArrayBuffer(blob);
    });
}

// const createAudioInputStream = (audioBlob: Blob): sdk.AudioInputStream => {

  
//   // const stream = sdk.AudioInputStream.createPushStream();
  
//   // const reader = new FileReader();
//   // reader.onload = () => {
//   //   const arrayBuffer = reader.result as ArrayBuffer;
//   //   const uint8Array = new Uint8Array(arrayBuffer);
//   //   stream.write(arrayBuffer);
//   //    stream.close();
//   // };
  
//   // reader.onerror = () => {
//   //   console.error('Error occurred while reading Blob as ArrayBuffer.');
//   // };
  
//   // reader.readAsArrayBuffer(audioBlob);
  
//   // return stream;
// };
  async function Pronunciation2(text: string,a:Blob): Promise<TPronunciationResult> {
    console.log('Pronunciation2',a)
    var pronunciationAssessmentConfig =
      sdk.PronunciationAssessmentConfig.fromJSON(
        `{"referenceText":"${text}","gradingSystem":"HundredMark","granularity":"Phoneme","phonemeAlphabet":"IPA","nBestPhonemeCount":5}`
      );

    //const audioConfig = sdk.AudioConfig.fromDefaultMicrophoneInput();
    const stream = sdk.AudioInputStream.createPushStream();
    blobToArrayBuffer(a).then((arrayBuffer) => {
      
        stream.write(arrayBuffer);
    }).finally(() => {
        stream.close();
    })


     
    const audioConfig = sdk.AudioConfig.fromStreamInput(stream)
   
    const recognizer = new sdk.SpeechRecognizer(speechConfig, audioConfig);

    pronunciationAssessmentConfig.applyTo(recognizer);

    let pronunciationResult: TPronunciationResult = null;

    setStatus("processing");
    recognizer.recognizeOnceAsync(
      (result) => {
        switch (result.reason) {
          case sdk.ResultReason.RecognizedSpeech:
            console.log(`RECOGNIZED: Text=${result.text}`);
            pronunciationResult =
              sdk.PronunciationAssessmentResult.fromResult(result);
            console.log("pronunciationResult", pronunciationResult);
            break;
          case sdk.ResultReason.NoMatch:
            console.log("NOMATCH: Speech could not be recognized.");
            setStatus("error");
            break;
          case sdk.ResultReason.Canceled:
            const cancellation = sdk.CancellationDetails.fromResult(result);
            console.log(`CANCELED: Reason=${cancellation.reason}`);

            if (cancellation.reason == sdk.CancellationReason.Error) {
              setStatus("error");
              console.log(`CANCELED: ErrorCode=${cancellation.ErrorCode}`);
              console.log(
                `CANCELED: ErrorDetails=${cancellation.errorDetails}`
              );
              console.log(
                "CANCELED: Did you set the speech resource key and region values?"
              );
            }
            break;
        }

        recognizer.close();
      },
      (err) => {
        console.trace("err - " + err);
        setStatus("error");
        recognizer.close();
      }
    );

    await waitUntil(() => pronunciationResult !== null);
    if (pronunciationResult) {
      setStatus("finished");
    } else {
      setStatus("error");
    }
 
    return pronunciationResult;
  }

  // Speech to text
  function SpeechToText() {
    const recognizer = new sdk.SpeechRecognizer(speechConfig);

    recognizer.recognizeOnceAsync(
      (result) => {
        console.log(`RECOGNIZED: Text=${result.text}`);
        // text = result.text;
        setStatus("idle");
        recognizer.close();
      },
      (err) => {
        console.trace("err - " + err);
        setStatus("error");
        setStatus("idle");
        recognizer.close();
      }
    );
  }

  return {
    TextToSpeech,
    Pronunciation,
    Pronunciation2,
    SpeechToText,
    status,
  };
}
