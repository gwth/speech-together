// @ts-nocheck

import * as sdk from "microsoft-cognitiveservices-speech-sdk";
import * as fs from "fs";
import { openPushStream } from "../filePushSteam";
import * as _ from "lodash";
import * as difflib from "difflib";
import * as path from "path";

export default function Pronunciation() {
  // pronunciation assessment with audio streaming and continue mode

  // now create the audio-config pointing to our stream and
  // the speech config specifying the language.
  // const filePath = path.join(__dirname, "./public/audio/someFile.wav");
  const filePath = path.join(process.cwd(), "./public/audio/", "someFile" + ".wav");
  console.log("🚀 ~ file: v3.ts:17 ~ Pronunciation ~ filePath:", filePath);
  const audioStream = openPushStream("./public/audio/" + "someFile" + ".wav");

  const audioConfig = sdk.AudioConfig.fromStreamInput(audioStream);
  const speechConfig = sdk.SpeechConfig.fromSubscription("682284bdc105420da6dbd696154d89ca", "eastasia");

  const reference_text =
    "Hello, my name is Suda. I'm from the city. I'm 25 years old. I work in an office. I like reading books and watching movies. I also enjoy going for walks in the park. It’s nice to meet you!";

  // create pronunciation assessment config, set grading system, granularity, and if enable miscue based on your requirement.
  const pronunciationAssessmentConfig = new sdk.PronunciationAssessmentConfig(
    reference_text,
    sdk.PronunciationAssessmentGradingSystem.HundredMark,
    sdk.PronunciationAssessmentGranularity.Phoneme,
    true
  );

  // setting the recognition language to English.
  speechConfig.speechRecognitionLanguage = "en-US";

  // create the speech recognizer.
  const reco = new sdk.SpeechRecognizer(speechConfig, audioConfig);
  pronunciationAssessmentConfig.applyTo(reco);

  const scoreNumber = {
    accuracyScore: 0,
    fluencyScore: 0,
    compScore: 0,
  };
  const allWords: sdk.Word[] = [];
  const currentText: string[] = [];
  let startOffset = 0;
  const recognizedWords: sdk.Word[] = [];
  const fluencyScores: number[] = [];
  const durations: number[] = [];
  let jo: any = {};

  // Before beginning speech recognition, setup the callbacks to be invoked when an event occurs.

  // The event recognizing signals that an intermediate recognition result is received.
  // You will receive one or more recognizing events as a speech phrase is recognized, with each containing
  // more recognized speech. The event will contain the text for the recognition since the last phrase was recognized.
  reco.recognizing = (s, e) => {
    const str = `(recognizing) Reason: ${sdk.ResultReason[e.result.reason]} Text: ${e.result.text}`;
    console.log(str);
  };

  // The event recognized signals that a final recognition result is received.
  // This is the final event that a phrase has been recognized.
  // For continuous recognition, you will get one recognized event for each phrase recognized.
  reco.recognized = (s, e) => {
    console.log("pronunciation assessment for: ", e.result.text);
    const pronunciation_result = sdk.PronunciationAssessmentResult.fromResult(e.result);
    console.log(
      " Accuracy score: ",
      pronunciation_result.accuracyScore,
      "\n",
      "pronunciation score: ",
      pronunciation_result.pronunciationScore,
      "\n",
      "completeness score : ",
      pronunciation_result.completenessScore,
      "\n",
      "fluency score: ",
      pronunciation_result.fluencyScore
    );

    jo = JSON.parse(e.result.properties.getProperty(sdk.PropertyId.SpeechServiceResponse_JsonResult));
    startOffset = jo.NBest[0].Words[0].Offset;
    const localtext = jo.NBest[0].Words.map((item: sdk.Word) => item.Word.toLowerCase());
    currentText.push(...localtext);
    fluencyScores.push(jo.NBest[0].PronunciationAssessment.FluencyScore);

    const isSucceeded = jo.RecognitionStatus === "Success";
    const nBestWords = jo.NBest[0].Words;
    const durationList: number[] = [];

    nBestWords.forEach((word: sdk.Word) => {
      recognizedWords.push(word);
      durationList.push(word.Duration);
    });

    durations.push(_.sum(durationList));

    if (isSucceeded && nBestWords) {
      allWords.push(...nBestWords);
    }
  };

  function calculateOverallPronunciationScore() {
    const resText = currentText.join(" ");
    let wholelyricsArry: string[] = [];
    let resTextArray: string[] = [];

    // The sample code provides only zh-CN and en-US locales
    if (["zh-cn"].includes("en-US")) {
      const resTextProcessed = (resText.toLowerCase() ?? "").replace(new RegExp("[^a-zA-Z0-9\u4E00-\u9FA5']+", "g"), " ");
      const wholelyrics = (reference_text.toLowerCase() ?? "").replace(new RegExp("[^a-zA-Z0-9\u4E00-\u9FA5']+", "g"), " ");
      const segment = new Segment();
      segment.useDefault();
      segment.loadDict("wildcard.txt");
      _.map(segment.doSegment(wholelyrics, { stripPunctuation: true }), (res) => wholelyricsArry.push(res["w"]));
      _.map(segment.doSegment(resTextProcessed, { stripPunctuation: true }), (res) => resTextArray.push(res["w"]));
    } else {
      let resTextProcessed = (resText.toLowerCase() ?? "")
        .replace(new RegExp('[!"#$%&()*+,-./:;<=>?@[^_`{|}~]+', "g"), "")
        .replace(new RegExp("]+", "g"), "");
      let wholelyrics = (reference_text.toLowerCase() ?? "")
        .replace(new RegExp('[!"#$%&()*+,-./:;<=>?@[^_`{|}~]+', "g"), "")
        .replace(new RegExp("]+", "g"), "");
      wholelyricsArry = wholelyrics.split(" ");
      resTextArray = resTextProcessed.split(" ");
    }
    const wholelyricsArryRes = _.map(
      _.filter(wholelyricsArry, (item) => !!item),
      (item) => item.trim()
    );

    // For continuous pronunciation assessment mode, the service won't return the words with `Insertion` or `Omission`
    // We need to compare with the reference text after receiving all recognized words to get these error words.
    const diff = new difflib.SequenceMatcher(null, wholelyricsArryRes, resTextArray);
    const lastWords: sdk.Word[] = [];

    for (const d of diff.getOpcodes()) {
      if (d[0] == "insert" || d[0] == "replace") {
        if (["zh-cn"].includes("en-US")) {
          for (let j = d[3], count = 0; j < d[4]; count++) {
            let len = 0;
            let bfind = false;

            _.map(allWords, (item, index) => {
              if (
                len == j ||
                (index + 1 < allWords.length && allWords[index].Word.length > 1 && j > len && j < len + allWords[index + 1].Word.length && !bfind)
              ) {
                const wordNew = _.cloneDeep(allWords[index]);

                if (allWords && allWords.length > 0 && allWords[index].PronunciationAssessment.ErrorType !== "Insertion") {
                  wordNew.PronunciationAssessment.ErrorType = "Insertion";
                }
                lastWords.push(wordNew);
                bfind = true;
                j += allWords[index].Word.length;
              }
              len = len + item.Word.length;
            });
          }
        } else {
          for (let j = d[3]; j < d[4]; j++) {
            if (allWords && allWords.length > 0 && allWords[j].PronunciationAssessment.ErrorType !== "Insertion") {
              allWords[j].PronunciationAssessment.ErrorType = "Insertion";
            }
            lastWords.push(allWords[j]);
          }
        }
      }
      if (d[0] == "delete" || d[0] == "replace") {
        if (d[2] == wholelyricsArryRes.length && !(jo.RecognitionStatus == "Success" || jo.RecognitionStatus == "Failed")) continue;
        for (let i = d[1]; i < d[2]; i++) {
          const word = {
            Word: wholelyricsArryRes[i],
            PronunciationAssessment: {
              ErrorType: "Omission",
            },
          };
          lastWords.push(word);
        }
      }
      if (d[0] == "equal") {
        for (let k = d[3], count = 0; k < d[4]; count++) {
          if (["zh-cn"].includes("en-US")) {
            let len = 0;
            let bfind = false;
            _.map(allWords, (item, index) => {
              if (len >= k && !bfind) {
                if (allWords[index].PronunciationAssessment.ErrorType !== "None") {
                  allWords[index].PronunciationAssessment.ErrorType = "None";
                }
                lastWords.push(allWords[index]);
                bfind = true;
                k += allWords[index].Word.length;
              }
              len = len + item.Word.length;
            });
          } else {
            lastWords.push(allWords[k]);
            k++;
          }
        }
      }
    }

    let reference_words: sdk.Word[] = [];

    if (["zh-cn"].includes("en-US")) {
      reference_words = allWords;
    } else {
      reference_words = wholelyricsArryRes.map((word) => ({
        Word: word,
        PronunciationAssessment: {
          ErrorType: "None",
        },
      }));
    }

    let recognizedWordsRes: sdk.Word[] = [];
    _.forEach(recognizedWords, (word) => {
      if (word.PronunciationAssessment.ErrorType == "None") {
        recognizedWordsRes.push(word);
      }
    });

    let compScore = Number(((recognizedWordsRes.length / reference_words.length) * 100).toFixed(0));

    if (compScore > 100) {
      compScore = 100;
    }

    scoreNumber.compScore = compScore;

    const accuracyScores: number[] = [];
    _.forEach(lastWords, (word) => {
      if (word && word?.PronunciationAssessment?.ErrorType != "Insertion") {
        accuracyScores.push(Number(word?.PronunciationAssessment.AccuracyScore ?? 0));
      }
    });

    scoreNumber.accuracyScore = Number((_.sum(accuracyScores) / accuracyScores.length).toFixed(0));

    if (startOffset) {
      const sumRes: number[] = [];
      _.forEach(fluencyScores, (x, index) => {
        sumRes.push(x * durations[index]);
      });

      scoreNumber.fluencyScore = _.sum(sumRes) / _.sum(durations);
    }

    const sortScore = Object.keys(scoreNumber).sort(function (a, b) {
      return scoreNumber[a] - scoreNumber[b];
    });

    if (jo.RecognitionStatus == "Success" || jo.RecognitionStatus == "Failed") {
      scoreNumber.pronScore = Number(
        (scoreNumber[sortScore["0"]] * 0.4 + scoreNumber[sortScore["1"]] * 0.4 + scoreNumber[sortScore["2"]] * 0.2).toFixed(0)
      );
    } else {
      scoreNumber.pronScore = Number((scoreNumber.accuracyScore * 0.5 + scoreNumber.fluencyScore * 0.5).toFixed(0));
    }

    console.log(
      "    Paragraph accuracy score: ",
      scoreNumber.accuracyScore,
      ", completeness score: ",
      scoreNumber.compScore,
      ", fluency score: ",
      scoreNumber.fluencyScore
    );

    _.forEach(lastWords, (word, ind) => {
      console.log(
        "    ",
        ind + 1,
        ": word: ",
        word.Word,
        "\taccuracy score: ",
        word.PronunciationAssessment.AccuracyScore,
        "\terror type: ",
        word.PronunciationAssessment.ErrorType,
        ";"
      );
    });
  }

  // The event signals that the service has stopped processing speech.
  reco.canceled = (s, e) => {
    console.log(e);
    if (e.reason === sdk.CancellationReason.Error) {
      const str = `(cancel) Reason: ${sdk.CancellationReason[e.reason]}: ${e.errorDetails}`;
      console.log(str);
    }
    reco.stopContinuousRecognitionAsync();
  };

  // Signals that a new session has started with the speech service
  reco.sessionStarted = (s, e) => {};

  // Signals the end of a session with the speech service.
  reco.sessionStopped = (s, e) => {
    reco.stopContinuousRecognitionAsync();
    reco.close();
    calculateOverallPronunciationScore();
  };

  reco.startContinuousRecognitionAsync();
}
