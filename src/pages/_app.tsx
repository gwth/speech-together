import "@/styles/globals.css";
import type { AppProps } from "next/app";
import store from "@/slices/store";
import { Provider } from "react-redux";
import { createTheme } from "@mui/material/styles";
import { ThemeProvider } from "@mui/material/styles";
// Layout
import DefaultLayout from "@/components/layout/DefaultLayout";
import DemoLayout from "@/components/layout/DemoLayout";
import { Inter, Sarabun } from "next/font/google";
import Head from "next/head";
const sarabun = Sarabun({ subsets: ["latin"], weight: "300", style: ["normal"], variable: "--font-sarabun" });

const theme = createTheme({
  typography: {
    fontFamily: sarabun.style.fontFamily,
  },
});
export default function App({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}>
      {/* <DefaultLayout /> */}
      <ThemeProvider theme={theme}>
        <main className={`${sarabun.variable} font-sarabun`}>
          <Component {...pageProps} />
        </main>
      </ThemeProvider>
    </Provider>
  );
}
