import { NextApiRequest, NextApiResponse } from "next";
import path from "path";
import fs from "fs";

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  try {
    if (req.method === "GET") {
      const { filename } = req.query;

      const filePath = path.join(process.cwd(), "./public/audio/", (filename as string) + ".wav");
      const stat = fs.statSync(filePath);

      res.writeHead(200, {
        "Content-Type": "audio/wav",
        "Content-Length": stat.size,
      });

      const readStream = fs.createReadStream(filePath);
      readStream.pipe(res);
    }
  } catch (error) {
    console.error("Error serving audio:", error);
    res.status(500).json({ error: "An error occurred while serving audio" });
  }
}
