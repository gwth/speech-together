import { NextApiRequest, NextApiResponse } from "next";
import { IncomingForm } from "formidable";
import { convertWebMBlobToWavAndSave } from "@/utils/convert-webm-to-wav";

export const config = {
  api: {
    bodyParser: false,
  },
};

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  try {
    if (req.method === "POST") {
      const result = await ProcessAudio(req);
      res.status(200).json({ message: "Conversion and save successful", filename: result });
    } else {
      res.status(405).json({ error: "Method not allowed" });
    }
  } catch (error) {
    console.error("Error converting and saving:", error);
    res.status(500).json({ error: "An error occurred during conversion and save" });
  }
}

async function ProcessAudio(req: NextApiRequest): Promise<string> {
  return new Promise<string>(async (resolve, reject) => {
    const form = new IncomingForm();

    form.parse(req, async (err, fields, files) => {
      if (err) {
        throw err;
      }
      //@ts-ignore
      const webmFile = files.webmFile as formidable.File;
      const outputDirectory = "./public/audio"; // Provide the path to the output directory
      convertWebMBlobToWavAndSave(webmFile[0].filepath, outputDirectory)
        .then((result) => {
          resolve(result);
        })
        .catch((err) => {
          reject(err);
        });
    });
  });
}
