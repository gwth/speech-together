import Pronunciation from "@/hooks/microsoft/v3";
import { NextApiRequest, NextApiResponse } from "next";

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  try {
    if (req.method === "GET") {
      const { filename, text } = req.query;

      Pronunciation();

      res.status(200).json({ filename, text });
    }
  } catch (error) {
    console.error("Error processing pronunciation:", error);
    res.status(500).json({ error: "An error occurred while processing pronunciation" });
  }
}
