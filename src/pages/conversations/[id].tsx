import React, { useEffect, useState } from "react";

// components
import Preview from "@/components/microsoft/Preview";
import Playground from "@/components/microsoft/Playground";
import { ButtonCustom } from "@/components/reuseable/button/Button.styled";

// mui
import Button from "@mui/material/Button";
import Container from "@mui/material/Container";
import Collapse from "@mui/material/Collapse";
import Slide from "@mui/material/Slide";
import Nossr from "@mui/material/NoSsr";

type Props = {};

export default function Conversation({}: Props) {
  const [toggle, setToggle] = useState<boolean>(false);

  return (
    <>
      <Nossr>
        <Container maxWidth="lg">
          {!toggle ? (
            <div>
              <div>
                <Preview />
              </div>
              <div className="mt-5 m-2 flex justify-center">
                {/* <Button variant="contained" onClick={() => setToggle(!toggle)}>
                  Start
                </Button> */}
                <ButtonCustom onClick={() => setToggle(!toggle)} />
              </div>
            </div>
          ) : (
            <div>
              <Playground />
            </div>
          )}
        </Container>
      </Nossr>
    </>
  );
}
