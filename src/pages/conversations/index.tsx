import React from "react";
import Link from "next/link";

import { conversations } from "@/hooks/conversation";
import type { Conversation } from "@/hooks/conversation";

import { useRouter } from "next/router";

// mui
import Container from "@mui/material/Container";
import Card from "@mui/material/Card";
import Paper from "@mui/material/Paper";

export default function Conversations() {
  const router = useRouter();
  const [conversation, setConversation] =
    React.useState<Conversation[]>(conversations);

  const handleClick = (id: number) => {
    router.push(`/conversations/${id}`);
  };

  return (
    <>
      <Container maxWidth="lg">
        <div className="text-4xl font-bold flex justify-center my-10">
          Conversations
        </div>
        <div className="flex">
          {conversation.map((item, index) => (
            <Paper
              elevation={24}
              className="p-2 m-2 cursor-pointer w-40 h-40 text-center transition duration-150 ease-in-out hover:scale-105 rounded-xl flex justify-center items-center bg-gray-200"
              onClick={() => handleClick(item.id)}
              key={index}>
              <div>{item.name}</div>
            </Paper>
          ))}
        </div>
      </Container>
    </>
  );
}
