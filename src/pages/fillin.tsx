import React, { useState } from "react";
import {
  Button,
  Container,
  CssBaseline,
  Divider,
  Grid,
  Paper,
  Typography,
} from "@mui/material";
import FillInTheBlankParagraph from "../components/FillInTheBlank";
import { useRef } from "react";
import SendIcon from "@mui/icons-material/Send";
import Head from "next/head";
import PopupDialog from "../components/PopupDialog";
const fillin: React.FC = () => {
  const paragraph =
    "Once upon a time, in a <inputfield> far away, there lived a brave <inputfield> named <inputfield>. " +
    "They embarked on a journey to find the <inputfield> and bring peace to their land.";
  const answers = ["kingdom", "knight", "Lancelot", "Holy Grail"];

  const p2 = `My favorite fruit is <inputfield> .`;
  const a2 = ["apple"];

  const p3 = `I live in a <inputfield>.`;
  const a3 = ["house"];

  const p4 = `The sky is <inputfield> today.`;
  const a4 = ["blue"];
  const p5 = `My name is Tim. I’m thirteen <inputfield>. I’m in the eighth grade. I have an older brother who is fifteen and is in the tenth grade. We go to the same school. In the mornings, we ride the <inputfield> together. It picks us up at 6:30 AM.`;
  const a5 = ["years old", "school bus"];

  const ref = useRef<any>();
  const ref2 = useRef<any>();
  const ref3 = useRef<any>();
  const ref4 = useRef<any>();
  const ref5 = useRef<any>();
  const [score, setScore] = useState<number>(0);
  const [open, setOpen] = useState(false);
  const handleOpen = () => {
    calScore();
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    if (score < 99) window.location.reload();
  };
  const calScore = () => {
    // ref.current.handleSubmit();
    let scoret: number =
      ref2.current.handleSubmit() +
      ref3.current.handleSubmit() +
      ref4.current.handleSubmit();
    // ref5.current.handleSubmit();
    setScore(scoret);
    console.log(scoret);
    // alert("คะแนนที่ได้ " + scoret + " คะแนน");
  };
  function getText() {
    if (score < 99) return "คะแนนที่ทำได้ " + score.toString() + " คะแนน";
    else {
      return "กรุณาทำข้อสอบให้ครบ";
    }
  }
  return (
    <>
      <Head>
        <title>4.2.4 ข้อสอบทักษะการเขียนภาษาอังกฤษ (Typing)</title>
      </Head>
      <Container component="main" maxWidth="lg">
        <Grid container>
          <Grid xs={12} item className=" flex justify-between my-10">
            <Typography
              sx={{}}
              color="text.secondary"
              gutterBottom
              className="text-black font-semibold "
              variant="h5"
            >
              4.2.4 ข้อสอบทักษะการเขียนภาษาอังกฤษ (แบบคำเดียว)
            </Typography>
            <Button
              onClick={() => {
                handleOpen();
              }}
              variant="contained"
              startIcon={<SendIcon />}
            >
              Send
            </Button>
          </Grid>
          <Divider sx={{ bgcolor: "rgba(140, 140, 140, 1)", width: "100%" }} />
          {/* <Grid className="pt-6" item xs={12}>
            <Paper
              style={{
                backgroundColor: "rgba(0, 0, 0, 0.7)",
              }}
              className="p-5"
            >
              <FillInTheBlankParagraph
                ref={ref}
                paragraph={paragraph}
                blanks={answers.length}
                answers={answers}
                pathImg=""
              />
            </Paper>
          </Grid> */}
          <div className="ps-5 w-[100%]">
            <Grid className="pt-6" item xs={12}>
              {/* <Paper
                style={{
                  backgroundColor: "rgba(242, 242, 242, 0.7)",
                }}
                className="p-5  rounded-xl "
              >
              </Paper> */}
              <h2 className="text-black ">Type in the blank.</h2>
            </Grid>

            <Grid className="pt-6" item xs={12}>
              <Paper
                style={{
                  backgroundColor: "rgba(242, 242, 242, 0.7)",
                }}
                className="p-5  rounded-xl"
              >
                <FillInTheBlankParagraph
                  ref={ref2}
                  paragraph={p2}
                  blanks={a2.length}
                  answers={a2}
                  pathImg="./images/apple.png"
                />
              </Paper>
            </Grid>

            <Grid className="pt-6" item xs={12}>
              <Paper
                style={{
                  backgroundColor: "rgba(242, 242, 242, 0.7)",
                }}
                className="p-5 rounded-xl"
              >
                <FillInTheBlankParagraph
                  ref={ref3}
                  paragraph={p3}
                  blanks={a3.length}
                  answers={a3}
                  pathImg="./images/house.png"
                />
              </Paper>
            </Grid>
            <Grid className="pt-6 pb-5" item xs={12}>
              <Paper
                style={{
                  backgroundColor: "rgba(242, 242, 242, 0.7)",
                }}
                className="p-5 rounded-xl"
              >
                <FillInTheBlankParagraph
                  ref={ref4}
                  paragraph={p4}
                  blanks={a4.length}
                  answers={a4}
                  pathImg="./images/blue sky.png"
                />
              </Paper>
            </Grid>
          </div>
          {/* <Grid className="pt-6" item xs={12}>
            <Paper
              style={{
                backgroundColor: "rgba(0, 0, 0, 0.7)",
              }}
              className="p-5 rounded-xl"
            >
              <FillInTheBlankParagraph
                ref={ref5}
                paragraph={p5}
                blanks={a5.length}
                answers={a5}
                pathImg=""
              />
            </Paper>
          </Grid> */}
        </Grid>
      </Container>
      <PopupDialog
        open={open}
        onClose={handleClose}
        // text={"คะแนนที่ทำได้ " + {{...score.toString()}} + "คะแนน"}
        text={getText()}
      />
    </>
  );
};

export default fillin;
