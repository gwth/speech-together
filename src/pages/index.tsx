import Image from "next/image";
import { Inter } from "next/font/google";
import DefaultLayout from "@/components/layout/DefaultLayout";
import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardActions from "@mui/material/CardActions";
import Typography from "@mui/material/Typography";
import { useRouter } from "next/router";
import { Divider, Grid, Paper, Stack } from "@mui/material";
import HomeDemo from "@/components/HomeDemo";
import Head from "next/head";
import DemoLayout from "@/components/layout/DemoLayout";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  const router = useRouter();
  return (
    <>
      <Head>
        {/* <title>Demo รูปแบบของข้อสอบ</title> */}
        <title>Demo </title>
        {/* <link rel="icon" href="/images/Chaiyaphum_Rjbht._Univ.png" /> */}
      </Head>
      <DemoLayout title="Demo " />
      {/* <DemoLayout title="Demo รูปแบบของข้อสอบ" showLogo /> */}
      <HomeDemo />
    </>

    // <Stack spacing={2} className="m-10">
    //   <h1 className="text-white mb-2">HOME</h1>
    //   <Divider sx={{ bgcolor: "white" }} />
    //   <Grid container justifyContent="center" spacing={3}>
    //     <Grid item>
    //       <Card
    //         sx={{ minWidth: 200, maxWidth: 500 }}
    //         className="cursor-pointer transition duration-150 ease-in-out hover:scale-105 rounded-xl"
    //         onClick={() => router.push("/vocabulary3")}
    //       >
    //         <CardContent>
    //           <Typography
    //             sx={{ fontSize: 20 }}
    //             color="text.secondary"
    //             gutterBottom
    //           >
    //             Speaking
    //           </Typography>
    //         </CardContent>
    //       </Card>
    //     </Grid>
    //     <Grid item>
    //       {" "}
    //       <Card
    //         sx={{ minWidth: 100, maxWidth: 500 }}
    //         className="cursor-pointer transition duration-150 ease-in-out hover:scale-105 rounded-xl"
    //         onClick={() => router.push("/type/listening")}
    //       >
    //         <CardContent>
    //           <Typography
    //             sx={{ fontSize: 20 }}
    //             color="text.secondary"
    //             gutterBottom
    //           >
    //             Listening
    //           </Typography>
    //         </CardContent>
    //       </Card>
    //     </Grid>
    //     <Grid item>
    //       {" "}
    //       <Card
    //         sx={{ minWidth: 100, maxWidth: 500 }}
    //         className="cursor-pointer transition duration-150 ease-in-out hover:scale-105 rounded-xl"
    //         onClick={() => router.push("/type/reading")}
    //       >
    //         <CardContent>
    //           <Typography
    //             sx={{ fontSize: 20 }}
    //             color="text.secondary"
    //             gutterBottom
    //           >
    //             Reading
    //           </Typography>
    //         </CardContent>
    //       </Card>
    //     </Grid>
    //   </Grid>

    //   {/* <Card
    //       sx={{ minWidth: 100, maxWidth: 500 }}
    //       className="cursor-pointer transition duration-150 ease-in-out hover:scale-105 rounded-xl"
    //       onClick={() => router.push("/vocabulary")}
    //     >
    //       <CardContent>
    //         <Typography
    //           sx={{ fontSize: 20 }}
    //           color="text.secondary"
    //           gutterBottom
    //         >
    //           Demo Vocabulary 17
    //         </Typography>
    //       </CardContent>
    //     </Card>
    //     <Card
    //       sx={{ minWidth: 100, maxWidth: 500 }}
    //       className="cursor-pointer transition duration-150 ease-in-out hover:scale-105 rounded-xl"
    //       onClick={() => router.push("/vocabulary2")}
    //     >
    //       <CardContent>
    //         <Typography
    //           sx={{ fontSize: 20 }}
    //           color="text.secondary"
    //           gutterBottom
    //         >
    //           Demo Vocabulary 18
    //         </Typography>
    //       </CardContent>
    //     </Card> */}
    //   {/* <Card
    //     sx={{ minWidth: 100, maxWidth: 500 }}
    //     className="cursor-pointer transition duration-150 ease-in-out hover:scale-105 rounded-xl"
    //     onClick={() => router.push("/conversations")}
    //   >
    //     <CardContent>
    //       <Typography
    //         sx={{ fontSize: 20 }}
    //         color="text.secondary"
    //         gutterBottom
    //       >
    //         Demo Conversations
    //       </Typography>
    //     </CardContent>
    //   </Card> */}
    // </Stack>
  );
}
