import React, { useState } from "react";
import Nossr from "@mui/material/NoSsr";
import Container from "@mui/material/Container";
import Paper from "@mui/material/Paper";
import IconButton from "@mui/material/IconButton";
import MicIcon from "@mui/icons-material/Mic";
import VolumeUpIcon from "@mui/icons-material/VolumeUp";
import Grid from "@mui/material/Grid";
import RadioGroup from "@mui/material/RadioGroup";
import Radio from "@mui/material/Radio";
import FormControlLabel from "@mui/material/FormControlLabel";
import Button from "@mui/material/Button";
const listening: React.FC = () => {
  const paragraph: string =
    "Piedmont, or mountain, glaciers are found in many parts of the world. In North America they are distributed along the mountain ranges of the Pacific Coast from central California northward. They abound in the Andes range in South America and are familiar and greatly admired spectacles in the Alps, the Pyrenees, the Caucasus Mountains and the mountains of Scandanavia. Rivers of ice flow down the valleys of various Asian mountain ranges, including the Himalayas, the Hindu Kush, and the Karakoram and Kunlun ranges. They are also a feature of the Southern Alps of New Zealand and are found in the lofty mountains of New Guinea. The largest piedmont glaciers are the Malaspina and Bering glaciers, both in Alaska.";
  const questions = [
    {
      question: "A What is the capital of France?",
      correctAnswer: "a",
      options: [
        { value: "a", text: "Paris" },
        { value: "b", text: "London" },
        { value: "c", text: "Berlin" },
        { value: "d", text: "Rome" },
      ],
      soundPath: "/sounds/Conversation_Eng-S2.mp3",
      selectAns: null,
    },
    {
      question: "B Which planet is known as the Red Planet?",
      correctAnswer: "b",
      options: [
        { value: "a", text: "Venus" },
        { value: "b", text: "Mars" },
        { value: "c", text: "Jupiter" },
        { value: "d", text: "Saturn" },
      ],
      soundPath: "/sounds/Conversation_Eng-S3.mp3",
      selectAns: null,
    },
    {
      question: "C What is the largest mammal?",
      correctAnswer: "c",
      options: [
        { value: "a", text: "Elephant" },
        { value: "b", text: "Giraffe" },
        { value: "c", text: "Blue Whale" },
        { value: "d", text: "Lion" },
      ],
      soundPath: "",
      selectAns: null,
    },
  ];
  const [userAnswers, setUserAnswers] = useState<string[]>([]);
  const handleRadioChange = (
    event: React.ChangeEvent<HTMLInputElement>,
    questionIndex: number
  ) => {
    const newAnswers = [...userAnswers];
    newAnswers[questionIndex] = event.target.value;
    setUserAnswers(newAnswers);
  };
  function sendAnswer() {
    console.log("sendAnswer");
    if (userAnswers.length == questions.length) {
      let score: number = 0;
      questions.forEach((element, index) => {
        element.correctAnswer == userAnswers[index] ? score++ : "";
      });
      alert("คะแนนที่ได้ " + score + " คะแนน");
      window.location.reload();
    } else {
      alert("กรุณาทำข้อสอบให้ครบ");
    }
  }
  const getParagraph = () => {
    if (paragraph != "") {
      return (
        <Paper
          className={
            "ms-6 p-2 m-2 pb-5  h-50 text-center transition duration-150 rounded-xl  items-center bg-gray-200"
          }
        >
          <h1>paragraph</h1>
          <div className="text-md self-center text-left pl-5 leading-8">
            {paragraph}
          </div>
        </Paper>
      );
    }
  };
  const getAudio = (index: number) => {
    if (questions[index].soundPath != "") {
      return (
        <div className="text-left ps-5 pb-2">
          <audio controls>
            <source src={questions[index].soundPath} type="audio/mpeg"></source>
            Your browser does not support the audio element.
          </audio>
        </div>
      );
    } else "";
  };
  return (
    <>
      <Nossr>
        <Container maxWidth="lg">
          <Grid container spacing={2}>
            <Grid
              xs={12}
              item
              className="text-4xl font-bold flex justify-between my-10"
            >
              <div> Listening</div>
              <Button onClick={sendAnswer} variant="contained">
                Send Answer
              </Button>
            </Grid>
            {getParagraph()}

            {questions.map((item, index: number) => {
              return (
                <Grid key={index} item xs={12}>
                  <Paper
                    className={
                      "p-2 m-2 pb-5  h-50 text-center transition duration-150 rounded-xl  items-center bg-gray-200"
                    }
                  >
                    <div className="flex-column justify-start mb-5 ">
                      <div className="text-md self-center text-left pl-5 leading-8">
                        <h3>{item.question}</h3>
                      </div>
                      {getAudio(index)}
                      <div className="text-md self-center text-left pl-5 leading-8">
                        <RadioGroup
                          defaultValue="outlined"
                          name="radio-buttons-group"
                          onChange={(event) => handleRadioChange(event, index)}
                        >
                          {/* {choice.text} */}
                          {item.options.map((choice, index2: number) => {
                            return (
                              <FormControlLabel
                                key={index2}
                                value={choice.value}
                                control={<Radio />}
                                label={choice.text}
                              />
                            );
                          })}
                        </RadioGroup>
                      </div>
                    </div>
                  </Paper>
                </Grid>
              );
            })}
          </Grid>
        </Container>
      </Nossr>
    </>
  );
};

export default listening;
