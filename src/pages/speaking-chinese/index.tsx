import React, { useEffect } from "react";
import Button from "@/components/speaking-test/Button";

import {
  IconArrowNarrowLeft,
  IconCheck,
  IconMicrophone,
  IconRefresh,
  IconSend,
  IconX,
} from "@tabler/icons-react";
import AudioPlayer from "@/components/speaking-test/AudioPlayer";
import RecordButton from "@/components/speaking-test/RecordButton";
import Listen from "@/components/speaking-test/Listen";
import Loading from "@/components/speaking-test/Loading";
import { useAudioRecorder } from "react-audio-voice-recorder";
import Listen2 from "@/components/speaking-test/Listen2";

const API_ENDPOINT = process.env.NEXT_PUBLIC_API_ENDPOINT;
const API_PORT = process.env.NEXT_PUBLIC_API_PORT;
type ReadingCoach = {
  id: string;
  word: string;
  url: string;
  state: "idle" | "loading" | "success";
  score: number;
  audioUrl: string;
  displayText: string;
};
interface IPronunciationResult {
  scoreNumber: {
    accuracyScore: number;
    fluencyScore: number;
    compScore: number;
    pronScore: number;
  };
  lastWords: Words[];
}
type Words = {
  Word: string;
  PronunciationAssessment: {
    AccuracyScore: number;
    ErrorType: string;
  };
};
export default function SpeakChinese() {
  const [pronunciationResult, setPronunciationResult] =
    React.useState<IPronunciationResult | null>(null);
  const {
    startRecording,
    stopRecording,
    togglePauseResume,
    recordingBlob,
    isRecording,
    isPaused,
    recordingTime,
    mediaRecorder,
  } = useAudioRecorder();
  const [readingCoachId, setReadingCoachingId] = React.useState("");
  const [microphoneAvailable, setMicrophoneAvailable] = React.useState(false);
  const [audioURL, setAudioURL] = React.useState<string>("");
  const [readingCoach, setReadingCoach] = React.useState<ReadingCoach[]>([
    {
      id: "1",
      score: 0,
      state: "idle",
      url: "",
      word: "你好.",
      audioUrl: "/sounds/chinese/Male_Chinese 1.mp3",
      displayText: "你好.",
    },
    {
      id: "2",
      score: 0,
      state: "idle",
      url: "",
      word: "你 好 吗 ?",
      audioUrl: "/sounds/chinese/Male_Chinese 2.mp3",
      displayText: "你好吗?",
    },
    {
      id: "3",
      score: 0,
      state: "idle",
      url: "",
      word: "再见.",
      audioUrl: "/sounds/chinese/Male_Chinese 3.mp3",
      displayText: "再见.",
    },
    {
      id: "4",
      score: 0,
      state: "idle",
      url: "",
      word: "谢谢.",
      audioUrl: "/sounds/chinese/Male_Chinese 4.mp3",
      displayText: "谢谢.",
    },
    {
      id: "5",
      score: 0,
      state: "idle",
      url: "",
      word: "对不起.",
      audioUrl: "/sounds/chinese/Male_Chinese 5.mp3",
      displayText: "对不起.",
    },
  ]);

  const handleRecordReadingCoach = (id: string) => {
    if (isRecording) {
      stopRecording();
      console.log("stopRecording");
      // if (!recordingBlob) return;
      // const url = URL.createObjectURL(recordingBlob);
      // setReadingCoach((prev) =>
      //   prev.map((rc) => (rc.id == id ? { ...rc, url } : rc))
      // );
    } else {
      setReadingCoach((prev) =>
        prev.map((rc) => (rc.id == id ? { ...rc, state: "loading" } : rc))
      );
      startRecording();
      setReadingCoachingId(id);
    }
  };
  const handleReadingCoach = (id: string, text: string) => {
    if (!recordingBlob) return;
    const formData = new FormData();
    formData.append("webmFile", recordingBlob, "input.webm");
    formData.append("referent_text", text);
    fetch(`${API_ENDPOINT}/api/uploadCN`, {
      method: "POST",
      body: formData,
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error("Failed to convert and save.");
        }
      })
      .then((data: IPronunciationResult) => {
        if (data.lastWords[0] === null) return;
        console.log("response", data);
        setReadingCoach((prev) =>
          prev.map((rc) =>
            rc.id == id
              ? {
                  ...rc,
                  score: data.scoreNumber.pronScore,
                  state: "success",
                }
              : rc
          )
        );
      })
      .catch((error) => {
        console.error("Error:", error);
      })
      .finally(() => {});
  };
  useEffect(() => {
    if (!recordingBlob) return;
    const url = URL.createObjectURL(recordingBlob);
    setAudioURL(url);

    console.log("readingCoachId", readingCoachId);

    if (readingCoachId) {
      setReadingCoach((prev) =>
        prev.map((rc) => (rc.id == readingCoachId ? { ...rc, url } : rc))
      );
    }
  }, [recordingBlob]);
  useEffect(() => {
    async function checkMicrophone() {
      try {
        const stream = await navigator.mediaDevices.getUserMedia({
          audio: true,
        });
        // If getUserMedia() successfully returns a stream, microphone is available
        setMicrophoneAvailable(true);
        stream.getTracks().forEach((track) => track.stop()); // Stop the stream
      } catch (error) {
        // If there's an error, microphone is not available
        setMicrophoneAvailable(false);
        console.error("Error accessing microphone:", error);
      }
    }

    checkMicrophone();
  }, []);
  return (
    <div className="bg-[#F5F5F5] h-screen flex flex-column flex-wrap justify-center relative">
      <nav className="absolute h-[70px] w-full bg-[#73738E] top-0 left-0 mx-auto z-10 ">
        <div className="flex justify-start items-center text-white text-4xl sm:w-[100%] md:w-[80%] xl:w-[60%] h-full mx-auto">
          Speaking Chinese Test
        </div>
      </nav>
      <div className="container  mx-auto pt-[100px] sm:w-[100%] md:w-[80%] xl:w-[60%] bg-[#F5F5F5] h-[calc(100vh - (100vh-70px))]  mx-0 p-10 relative">
        {!microphoneAvailable ? (
          <div className="bg-red-500 rounded-xl p-5 mb-5 ">
            Microphone is not available
          </div>
        ) : null}
        {readingCoach.map((w, index) => (
          <div
            key={index}
            className="flex flex-row items-center justify-start gap-2"
          >
            <div className="bg-white text-xl rounded-xl p-3 min-w-[150px] my-1 custom-box-shadow flex flex-row justify-between items-center">
              <div className="">{w.displayText}</div>
              <Listen2 audioUrl={w.audioUrl} />
            </div>

            <RecordButton
              isRecording={
                w.state == "loading" && w.id === readingCoachId && isRecording
              }
              onClick={() => handleRecordReadingCoach(w.id)}
            />

            {w.url && w.state == "loading" ? (
              <>
                <AudioPlayer src={w.url} />
                <Button
                  label="Submit"
                  iconProps={<IconSend color="white" />}
                  onClick={() => handleReadingCoach(w.id, w.word)}
                />
              </>
            ) : null}

            {w.score !== null && w.state == "success" ? (
              <div className="flex flex-row justify-start items-center gap-2 bg-white rounded-xl p-3 min-w-[150px] my-1 custom-box-shadow w-full">
                {w.score >= 80 ? (
                  <IconCheck color="green" size={25} />
                ) : (
                  <IconX color="red" size={25} />
                )}
                <div className="text-xl ">{w.score}% Correct</div>
              </div>
            ) : null}
          </div>
        ))}
      </div>
    </div>
  );
}
