import MessageBox from "@/components/speaking-test/MessageBox";
import Button from "@/components/speaking-test/Button";

import {
  IconArrowNarrowLeft,
  IconCheck,
  IconMicrophone,
  IconRefresh,
  IconSend,
  IconVolume,
  IconX,
  IconDownload,
} from "@tabler/icons-react";
import AudioPlayer from "@/components/speaking-test/AudioPlayer";
import RecordButton from "@/components/speaking-test/RecordButton";
import Listen from "@/components/speaking-test/Listen";
import Loading from "@/components/speaking-test/Loading";
import { useAudioRecorder } from "react-audio-voice-recorder";
import React, { useRef } from "react";
import { Tooltip } from "react-tooltip";
import { useMicrosoft } from "@/hooks/microsoft/v1";
import { TextField } from "@mui/material";
import { TypeChangeEvent } from "@/utils/types";

const API_ENDPOINT = process.env.NEXT_PUBLIC_API_ENDPOINT;
const API_PORT = process.env.NEXT_PUBLIC_API_PORT;

interface IPronunciationResult {
  scoreNumber: {
    accuracyScore: number;
    fluencyScore: number;
    compScore: number;
    pronScore: number;
  };
  lastWords: Words[];
}

type Words = {
  Word: string;
  PronunciationAssessment: {
    AccuracyScore: number;
    ErrorType: string;
  };
};

type ReadingCoach = {
  id: string;
  word: string;
  url: string;
  state: "setPassage" | "idle" | "loading" | "success";
  score: number;
};

// const paragraph: string = `I'm Sai. I'd like to tell you about my family. My mom has long brown hair and blue eyes. She's tall and wears glasses. My dad has short
//             black hair and brown eyes. He's also tall and has a beard.`;

function generateUID(): string {
  const alphanumeric =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  const uidLength = 16;
  let uid = "";

  for (let i = 0; i < uidLength; i++) {
    uid += alphanumeric.charAt(Math.floor(Math.random() * alphanumeric.length));
  }

  return uid;
}

export default function SpeakingTestSetPassage() {
  const [paragraph, setParagraph] =
    React.useState<string>(`I'm Sai. I'd like to tell you about my family. My mom has long brown hair and blue eyes. She's tall and wears glasses. My dad has short
            black hair and brown eyes. He's also tall and has a beard.`);
  const [inputValue, setInputValue] = React.useState("");
  const handleTextChange = (event: TypeChangeEvent) => {
    setInputValue(event.target.value);
  };
  const [pronunciationResult, setPronunciationResult] =
    React.useState<IPronunciationResult | null>(null);
  const [words, setWords] = React.useState<Words[]>([]);
  const [speechNotDetect, setSpeechNotDetect] = React.useState<boolean>(false);
  const [audioBlob, setAudioBlob] = React.useState<Blob | null>(null);
  const [audioURL, setAudioURL] = React.useState<string>("");

  const [stateFetch, setStateFetch] = React.useState<
    "setPassage" | "idle" | "loading" | "success" | "failed" | "lastPage"
  >("setPassage");
  const [microphoneAvailable, setMicrophoneAvailable] = React.useState(false);
  const [readingCoachId, setReadingCoachingId] = React.useState("");
  const [readingCoach, setReadingCoach] = React.useState<ReadingCoach[]>([
    /*  {
      id: "ss",
      score: 0,
      state: "idle",
      url: "",
      word: "family",
    },
    {
      id: "sss",
      score: 0,
      state: "idle",
      url: "",
      word: "brown",
    }, */
  ]);

  const { TextToSpeech, StopPlayer } = useMicrosoft();

  const {
    startRecording,
    stopRecording,
    togglePauseResume,
    recordingBlob,
    isRecording,
    isPaused,
    recordingTime,
    mediaRecorder,
  } = useAudioRecorder();

  // const paragraph = "How old are you?";

  React.useEffect(() => {
    if (!recordingBlob) return;
    const url = URL.createObjectURL(recordingBlob);
    setAudioURL(() => url);

    console.log("readingCoachId", readingCoachId);

    if (readingCoachId) {
      setReadingCoach((prev) =>
        prev.map((rc) => (rc.id == readingCoachId ? { ...rc, url } : rc))
      );
    }
  }, [recordingBlob]);

  /*   React.useEffect(() => {
    if (pronunciationResult === null) return;
    if (pronunciationResult.scoreNumber.pronScore < 50) {
      setAudioBlob(null);
    }
  }, [pronunciationResult]);
 */

  React.useEffect(() => {
    async function checkMicrophone() {
      try {
        const stream = await navigator.mediaDevices.getUserMedia({
          audio: true,
        });
        // If getUserMedia() successfully returns a stream, microphone is available
        setMicrophoneAvailable(true);
        stream.getTracks().forEach((track) => track.stop()); // Stop the stream
      } catch (error) {
        // If there's an error, microphone is not available
        setMicrophoneAvailable(false);
        console.error("Error accessing microphone:", error);
      }
    }

    checkMicrophone();
  }, []);

  const handleSubmit = () => {
    console.log("handleSubmit");
    if (!recordingBlob) return;
    const formData = new FormData();
    formData.append("webmFile", recordingBlob, "input.webm");
    setStateFetch("loading");
    formData.append("referent_text", paragraph);
    fetch(`${API_ENDPOINT}/api/upload`, {
      method: "POST",
      body: formData,
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error("Failed to convert and save.");
        }
      })
      .then((data) => {
        setSpeechNotDetect(data.lastWords[0] === null);
        if (data.scoreNumber.pronScore === null) {
          setStateFetch("failed");
          return;
        }
        if (data.lastWords[0] === null) return;

        console.log("response", data);
        const wordMispronunciation: Words[] = data.lastWords.filter(
          (m: Words) =>
            m.PronunciationAssessment.ErrorType == "Mispronunciation"
        );
        const readingCoach = wordMispronunciation.reduce<ReadingCoach[]>(
          (carry, w) => {
            const uuid = generateUID();
            const newWord: ReadingCoach = {
              id: uuid,
              score: 0,
              state: "idle",
              url: "",
              word: w.Word,
            };

            carry.push(newWord);
            return carry;
          },
          []
        );

        setReadingCoach(readingCoach);

        setPronunciationResult(data);
        setWords(data.lastWords);
        setStateFetch("success");

        // setAudioURL("");
      })
      .catch((error) => {
        setStateFetch("failed");
        console.error("Error:", error);
      })
      .finally(() => {});
  };

  const handleSubmitPassage = () => {
    setParagraph(inputValue);
    setStateFetch("idle");
  };

  const handleColor = (errorType: string) => {
    switch (errorType) {
      case "Omission":
        return "bg-[#A057FF]";
      case "Insertion":
        return "bg-[#26D9B5]";
      case "Mispronunciation":
        return "bg-[#D13535]";
      default:
        return "#000000";
    }
  };

  const countErr = () => {
    const mispronunciations = words.filter(
      (w) => w.PronunciationAssessment.ErrorType == "Mispronunciation"
    );
    const omission = words.filter(
      (w) => w.PronunciationAssessment.ErrorType == "Omission"
    );
    const insertions = words.filter(
      (w) => w.PronunciationAssessment.ErrorType == "Insertion"
    );
    const result = [
      {
        color: "#D13535",
        label: "Mispronunciations",
        count: mispronunciations.length,
      },
      { color: "#A057FF", label: "Omissions", count: omission.length },
      { color: "#26D9B5", label: "Insertions", count: insertions.length },
    ];

    return { result, mispronunciations };
  };

  const handleReadingCoach = (id: string, text: string) => {
    if (!recordingBlob) return;
    const formData = new FormData();
    formData.append("webmFile", recordingBlob, "input.webm");
    formData.append("referent_text", text);
    fetch(`${API_ENDPOINT}/api/upload`, {
      method: "POST",
      body: formData,
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error("Failed to convert and save.");
        }
      })
      .then((data: IPronunciationResult) => {
        if (data.lastWords[0] === null) return;
        console.log("response", data);
        setReadingCoach((prev) =>
          prev.map((rc) =>
            rc.id == id
              ? { ...rc, score: data.scoreNumber.pronScore, state: "success" }
              : rc
          )
        );
      })
      .catch((error) => {
        console.error("Error:", error);
      })
      .finally(() => {});
  };

  const handleRecordReadingCoach = (id: string) => {
    if (isRecording) {
      stopRecording();

      /*  if (!recordingBlob) return;
      const url = URL.createObjectURL(recordingBlob);
      setReadingCoach((prev) => prev.map((rc) => (rc.id == id ? { ...rc, url } : rc))); */
    } else {
      setReadingCoach((prev) =>
        prev.map((rc) => (rc.id == id ? { ...rc, state: "loading" } : rc))
      );
      startRecording();
      setReadingCoachingId(id);
    }
  };

  const handleReset = () => {
    setPronunciationResult(null);
    setWords([]);
    setSpeechNotDetect(false);
    setAudioURL("");
    // setStateFetch("idle");
    setStateFetch("setPassage");
    setReadingCoach([]);
  };

  function preStartRecording() {
    console.log("xx");
    StopPlayer();
    startRecording();
  }
  let audio: any = null;
  function playSoundRecord() {
    console.log("playSoundRecord");
    if (!audioURL) {
      return;
    }
    if (audio) {
      audio.pause();
    }
    audio = new Audio(audioURL);
    audio.currentTime = 0;
    audio.play();
  }
  return (
    <div className="bg-[#F5F5F5] h-screen flex flex-column flex-wrap justify-center relative">
      <nav className="absolute h-[70px] w-full bg-[#73738E] top-0 left-0 mx-auto z-10 ">
        <div className="flex justify-start items-center text-white text-4xl sm:w-[100%] md:w-[80%] xl:w-[60%] h-full mx-auto">
          Speaking Test Input Passage
        </div>
      </nav>
      <div className="container  mx-auto pt-[100px] sm:w-[100%] md:w-[80%] xl:w-[60%] bg-[#F5F5F5] h-[calc(100vh - (100vh-70px))]  mx-0 p-10 relative">
        {!microphoneAvailable ? (
          <div className="bg-red-500 rounded-xl p-5 mb-5 ">
            Microphone is not available
          </div>
        ) : null}
        {stateFetch == "loading" ? <Loading /> : null}
        {stateFetch == "failed" ? (
          <div className="custom-box-shadow h-5 rounded-xl bg-white p-4 absolute top-25 left-10 right-10 flex flex-row justify-start items-center gap-5">
            <button
              onClick={handleReset}
              type="button"
              className={`bg-red-500  border-0 rounded-lg h-10  w-12 p-1 px-3  flex flex-row justify-center items-center gap-1 cursor-pointer hover:bg-red-400 custom-box-shadow`}
            >
              <IconArrowNarrowLeft color="white" />
            </button>
            <div className="text-xl font-bold">Try Again</div>
          </div>
        ) : null}
        {stateFetch === "setPassage" ? (
          <div>
            <TextField
              id="outlined-multiline-static"
              label="Enter your passage"
              multiline
              rows={7}
              onChange={handleTextChange}
              defaultValue=""
              fullWidth
            />
            <div className="flex flex-row justify-between items-center py-6 gap-2">
              <div></div>
              <Button
                label="Submit Passage"
                iconProps={<IconSend color="white" />}
                onClick={handleSubmitPassage}
              />
            </div>
            {audioURL ? <AudioPlayer src={audioURL} /> : null}
          </div>
        ) : null}
        {stateFetch === "idle" ? (
          <div>
            <MessageBox
              passage={paragraph}
              onListen={() => {
                TextToSpeech(paragraph, false, true);
              }}
            />
            <div className="flex flex-row justify-between items-center py-6 gap-2">
              <RecordButton
                isRecording={isRecording}
                onClick={() =>
                  isRecording ? stopRecording() : preStartRecording()
                }
              />

              <Button
                label="Submit"
                iconProps={<IconSend color="white" />}
                onClick={handleSubmit}
                isDisabled={!recordingBlob}
              />
            </div>
            {audioURL ? <AudioPlayer src={audioURL} /> : null}
          </div>
        ) : null}

        {stateFetch == "success" ? (
          <div>
            <div className="flex flex-row justify-between items-center gap-2">
              <div className="text-2xl py-3">Result </div>
              <Button label="Home" onClick={handleReset} />
            </div>

            <div className="grid grid-cols-5 grid-rows-7 gap-5">
              <div className="col-span-2 ">
                <div className="custom-box-shadow h-fit  rounded-xl bg-white p-4">
                  <div className="underline underline-offset-1">Score</div>
                  <div className="text-5xl">
                    {pronunciationResult?.scoreNumber.compScore} %
                  </div>
                </div>
              </div>
              <div className="col-span-2 col-start-1 row-start-2">
                <div className="custom-box-shadow h-fit  rounded-xl bg-white p-4">
                  <div className="underline underline-offset-1">
                    Voice Record
                  </div>
                  <div className="text-5xl  mt-1  ">
                    <div className="flex flex-row justify-start items-center gap-4 py-2">
                      <button
                        onClick={playSoundRecord}
                        type="button"
                        className="bg-[#57BCE2]  border-0 rounded-lg h-9  w-9 p-1    flex flex-row justify-center items-center gap-1 cursor-pointer hover:bg-[#A6DEF2] custom-box-shadow"
                        // disabled={!audioURL}
                      >
                        <IconVolume color="white" size={20} />
                      </button>
                      <div className="text-xl">
                        {"Listen to My Voice Recording"}
                      </div>
                    </div>
                    <div className="flex flex-row justify-start items-center gap-4 py-2">
                      <button
                        onClick={() => {
                          if (!audioURL) return;
                          const link = document.createElement("a");
                          link.href = audioURL;
                          link.download = "recording.mp3";
                          document.body.appendChild(link);
                          link.click();
                          document.body.removeChild(link);
                        }}
                        // disabled={!audioURL}
                        type="button"
                        className="bg-[#57BCE2]  border-0 rounded-lg h-9  w-9 p-1    flex flex-row justify-center items-center gap-1 cursor-pointer hover:bg-[#A6DEF2] custom-box-shadow"
                      >
                        <IconDownload color="white" size={20} />
                      </button>
                      <div className="text-xl">
                        {"Download My Voice Recording"}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-span-2 row-span-2 col-start-1 row-start-3">
                <div className="custom-box-shadow  height-score-detail rounded-xl bg-white p-4">
                  {/*  <div className="pb-3">
                    <div className="underline underline-offset-1">Score</div>
                    <div className="text-5xl">{pronunciationResult?.scoreNumber.pronScore}</div>
                  </div> */}
                  <div>
                    <div className="underline underline-offset-1">Detail</div>
                    {countErr().result.map((item, index) => (
                      <div
                        key={index}
                        className="flex flex-row justify-start items-center gap-4 py-2"
                      >
                        <div
                          style={{ backgroundColor: item.color }}
                          className={`h-[40px] w-[40px] rounded-lg flex justify-center items-center text-xl font-bold text-white`}
                        >
                          {item.count}
                        </div>
                        <div className="text-xl">{item.label}</div>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
              <div className="col-span-2 col-start-1 row-start-5">
                <div className="custom-box-shadow h-fit  rounded-xl bg-white p-4">
                  <div className="underline underline-offset-1">
                    Reading coach
                  </div>
                  <div
                    className="text-5xl text-[#256AE1] mt-1 underline cursor-pointer"
                    onClick={() => setStateFetch("lastPage")}
                  >
                    {countErr().mispronunciations.length}
                  </div>
                </div>
              </div>

              <div className="col-span-3 row-span-5 col-start-3 row-start-1">
                <div className="custom-box-shadow   rounded-xl bg-white p-2 height-content-result">
                  {pronunciationResult !== null && (
                    <div className=" bg-white p-5 px-5   space-y-4  ">
                      {/* <div className="text-2xl uppercase text-[#BF4E5B] font-black">result</div> */}
                      <div className="flex flex-wrap">
                        <Tooltip
                          id="tooltip"
                          render={({ content }) => {
                            return <div>{content}</div>;
                          }}
                        />
                        {words.map((word, index) => (
                          <div
                            data-tooltip-id="tooltip"
                            data-tooltip-content={`${
                              word.PronunciationAssessment.ErrorType === "None"
                                ? "Accuracy score: " +
                                  word.PronunciationAssessment.AccuracyScore
                                : word.PronunciationAssessment.ErrorType
                            }`}
                            className={`inline px-1 m-[1px] cursor-pointer rounded-md ${handleColor(
                              word.PronunciationAssessment.ErrorType
                            )}`}
                            key={index}
                          >
                            {word.PronunciationAssessment.ErrorType ===
                              "Insertion" && (
                              <>
                                <span className="text-xl text-white font-normal line-through">
                                  {word.Word}
                                </span>
                              </>
                            )}

                            {word.PronunciationAssessment.ErrorType ===
                              "Omission" && (
                              <>
                                <span className="text-xl text-white font-normal ">
                                  {" "}
                                  {word.Word}{" "}
                                </span>
                              </>
                            )}

                            {word.PronunciationAssessment.ErrorType ===
                              "Mispronunciation" && (
                              <>
                                <span className="text-xl text-white font-normal">
                                  {word.Word}
                                </span>
                              </>
                            )}

                            {word.PronunciationAssessment.ErrorType ===
                              "None" && (
                              <>
                                <span className="text-xl font-normal">
                                  {word.Word}
                                </span>
                              </>
                            )}
                          </div>
                        ))}
                      </div>
                      {/* {JSON.stringify(countErr().mispronunciations.map((w) => w.Word))} */}
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        ) : null}

        {stateFetch == "lastPage" ? (
          <>
            <div className="text-2xl py-3">Reading coach</div>
            {readingCoach.map((w, index) => (
              <div
                key={index}
                className="flex flex-row items-center justify-start gap-2"
              >
                <div className="bg-white text-xl rounded-xl p-3 min-w-[150px] my-1 custom-box-shadow flex flex-row justify-between items-center">
                  <div className="">{w.word}</div>
                  <Listen
                    onClick={() => TextToSpeech(w.word, true)}
                    time={2000}
                  />
                </div>
                <RecordButton
                  isRecording={
                    w.state == "loading" &&
                    w.id === readingCoachId &&
                    isRecording
                  }
                  onClick={() => handleRecordReadingCoach(w.id)}
                />

                {w.url && w.state == "loading" ? (
                  <>
                    <AudioPlayer src={w.url} />
                    <Button
                      label="Submit"
                      iconProps={<IconSend color="white" />}
                      onClick={() => handleReadingCoach(w.id, w.word)}
                    />
                  </>
                ) : null}

                {w.score !== null && w.state == "success" ? (
                  <div className="flex flex-row justify-start items-center gap-2 bg-white rounded-xl p-3 min-w-[150px] my-1 custom-box-shadow w-full">
                    {w.score >= 80 ? (
                      <IconCheck color="green" size={25} />
                    ) : (
                      <IconX color="red" size={25} />
                    )}
                    <div className="text-xl ">{w.score}% Correct</div>
                  </div>
                ) : null}
              </div>
            ))}
            <div className="min-w-[150px] mb-2">
              <Button label="Finish" onClick={handleReset} />
            </div>
          </>
        ) : null}
      </div>
    </div>
  );
}
