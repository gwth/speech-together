import Container from "@mui/material/Container";

import Nossr from "@mui/material/NoSsr";
import React, { useState } from "react";
import Paper from "@mui/material/Paper";
import IconButton from "@mui/material/IconButton";
import MicIcon from "@mui/icons-material/Mic";
import VolumeUpIcon from "@mui/icons-material/VolumeUp";
import Grid from "@mui/material/Grid";
import { useMicrosoft } from "@/hooks/microsoft/v2";
//css
import styles from "@/styles/conversation.module.css";
import { IPronResult, IPronResultVocab } from "@/hooks/microsoft/types";

import CircularProgressWithLabel from "@/components/ProgressScore";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import Head from "next/head";
export default function speaking() {
  const [currentIndex, setCurrentIndex] = useState(99);
  const [pronResult, setPronResult] = React.useState<IPronResultVocab[]>([]);
  const [renderText, setRenderText] = React.useState<string>("");
  const mockVocab: string[] = [
    "Hello",
    "Goodbye",
    "Bedroom",
    "Raincoat",
    "I like pizza.",
    "She has a cat.",

    "Hello, my name is Suda. I'm from the city. I'm 25 years old. I work in an office. I like reading books and watching movies. I also enjoy going for walks in the park. It’s nice to meet you!",
  ];

  const mockCommand: string[] = [
    "Read the following words out loud.",
    "Read the following compound words out loud.",
    "Read the following sentences out loud.",
    "Read the following paragraphs out loud.",
  ];
  const { TextToSpeech, Pronunciation, status } = useMicrosoft();
  function clickVolumn(text: string) {
    TextToSpeech(text);
  }
  async function clickMic(text: string, index: number) {
    console.log("click mic");
    const pronResultFromFunc = await Pronunciation(text);
    console.log("mm", pronResultFromFunc);
    if (pronResultFromFunc) {
      setPronResult([
        ...pronResult,
        {
          accuracyScore: pronResultFromFunc.accuracyScore,
          fluencyScore: pronResultFromFunc.fluencyScore,
          pronunciationScore: pronResultFromFunc.pronunciationScore,
          completenessScore: pronResultFromFunc.completenessScore,
          index: index,
          detailResult: pronResultFromFunc.detailResult,
        },
      ]);
      //console.log("xx", pronResult);
    } else {
    }
  }
  const handleClickSetCurrent = (index: number) => {
    setCurrentIndex(index);
  };

  const loadingMicAndAudio = (index: number) => {
    if (currentIndex === index && status === "processing") {
      return <div className={`${styles["dot-flashing"]} self-center ms-10`} />;
    } else if (currentIndex === index && status === "error") {
      return (
        <div className="text-md self-center grow text-red-500 font-semibold flex justify-end">
          Try again
        </div>
      );
    } else "";
  };
  function getColor(value: number): string {
    switch (true) {
      case value >= 80:
        return "green";
      case value >= 50:
        return "orange";
      case value < 50:
        return "red";
    }
    return styles["dot-flashing"];
  }
  const showScore = (index: number) => {
    let x = pronResult.filter((f) => f.index === index);
    if (x.length > 0) {
      //console.log("ddd", x);

      //   return <div>accuracyScore {x[x.length - 1].accuracyScore} </div>;
      return (
        <div>
          <h4 className="text-left pl-5 pb-2 text-black">Assessment Result</h4>
          <Grid container>
            <Grid item xs={6} md={5} lg={3} className=" justify-center">
              <CircularProgressWithLabel
                value={x[x.length - 1].pronunciationScore}
                size={60}
                sx={{
                  color: getColor(x[x.length - 1].pronunciationScore),
                }}
              ></CircularProgressWithLabel>
              <p className="text-black">Pronunciation Score</p>
            </Grid>
          </Grid>
        </div>
      );
    } else "";
  };
  function getColorText(value: string): string {
    switch (value) {
      case "None":
        return "green";
      case "Mispronunciation":
        return "red";
    }
    return styles["dot-flashing"];
  }
  const showText = (index: number) => {
    let x = pronResult.filter((f) => f.index === index);
    if (x.length > 0) {
      let dataTemp = x[x.length - 1].detailResult.Words;
      // console.log("dataTemp", dataTemp);
      if (dataTemp.length > 0) {
        return (
          <div className="text-start pl-5 text-lg font-bold ">
            {dataTemp.map((item: any, index: number) => (
              <a
                // sx={{
                //   color: getColor(item.PronunciationAssessment.ErrorType),
                // }}
                style={{
                  color: getColorText(item.PronunciationAssessment.ErrorType),
                }}
                key={index}
              >
                {item.Word}{" "}
              </a>
            ))}
          </div>
        );
      }
    } else "";
  };
  const getCommand = (index: number) => {
    //0 2 4 6
    if (index == 0 || index == 2 || index == 4 || index == 6) {
      let text: string = "";
      switch (index) {
        case 0:
          text = mockCommand[0];
          break;
        case 2:
          text = mockCommand[1];
          break;
        case 4:
          text = mockCommand[2];
          break;
        case 6:
          text = mockCommand[3];
          break;

        default:
          break;
      }
      return (
        // <Paper
        //   style={{
        //     backgroundColor: "rgba(33, 58, 67, 1)",
        //   }}
        //   className={
        //     "p-2 mt-8 m-2 pb-5  h-50 text-center transition rounded-xl  items-center bg-gray-200"
        //   }
        // >
        <div className="flex justify-start  mt-8">
          <div className="text-md self-center text-left pl-5 leading-8 w-[100%]">
            <h2 className="text-black ">{text}</h2>
          </div>
        </div>
        // </Paper>
      );
    } else {
      return "";
    }
  };
  return (
    <>
      <Head>
        <title>4.2.2 ข้อสอบทักษะการพูดภาษาอังกฤษ</title>
      </Head>
      <Nossr>
        <Container maxWidth="lg">
          <Grid container>
            {/* <div className="text-2xl font-bold flex justify-center my-10 text-black">
              4.2.3 ข้อสอบ ทักษะการอ่านภาษาอังกฤษ
            </div> */}
            <Typography
              sx={{}}
              color="text.secondary"
              gutterBottom
              className="text-black my-10 font-semibold"
              variant="h5"
            >
              4.2.2 ข้อสอบทักษะการพูดภาษาอังกฤษ
            </Typography>
            <Divider
              sx={{ bgcolor: "rgba(140, 140, 140, 1)", width: "100%" }}
            />
            {mockVocab.map((item: string, index: number) => {
              return (
                <Grid key={index} item xs={12}>
                  {getCommand(index)}
                  <Paper
                    style={{
                      backgroundColor: "rgba(242, 242, 242, 0.7)",
                    }}
                    className={
                      "p-2 m-2 pb-5  h-50 text-center transition rounded-xl  items-center bg-gray-200"
                    }
                  >
                    <div className="flex justify-start mb-5  ">
                      <div className="text-md self-center text-left pl-5 leading-8 w-[100%]">
                        <h3 className="text-black ">{item}</h3>
                      </div>

                      <div className="flex ms-5 w-[30%] ">
                        <IconButton
                          className="ml-auto me-5"
                          onClick={async () => {
                            if (status != "processing") {
                              handleClickSetCurrent(index);
                              await clickMic(item, index);
                            }
                          }}
                        >
                          <MicIcon className="cursor-pointer text-black" />
                          <Typography className="ps-2  text-base text-black">
                            อัดเสียง
                          </Typography>
                        </IconButton>

                        {/* <IconButton
                          className=""
                          onClick={() => {
                            if (status != "processing") {
                              handleClickSetCurrent(index);
                              clickVolumn(item);
                            }
                          }}
                        >
                          <VolumeUpIcon className="cursor-pointer text-black" />
                          <Typography className=" text-base ps-2 text-black">
                            ฟังเฉลย
                          </Typography>
                        </IconButton> */}

                        {/* <IconButton
                          onClick={() => {
                            cc();
                          }}
                        >
                          <MicIcon className="cursor-pointer" />
                        </IconButton> */}
                      </div>
                    </div>
                    {loadingMicAndAudio(index)}
                    {showScore(index)}
                    {showText(index)}
                  </Paper>
                </Grid>
              );
            })}
          </Grid>
        </Container>
      </Nossr>
    </>
  );
}
