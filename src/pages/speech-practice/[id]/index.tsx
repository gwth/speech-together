import SpeechPracticeComp, { Paragraphs } from "@/components/SpeechPractice";
import { useRouter } from "next/router";

export default function PlaygroundSpeechPractice() {
  const router = useRouter();

  const { id } = router.query;

  const paragraph = Paragraphs.find((p) => p.id === parseInt(id as string));

  return (
    <div className="bg-[url('/speech-practice/background/bg-speech-practice.svg')] bg-cover min-h-screen">
      <SpeechPracticeComp title={paragraph?.title || ""} paragraph={paragraph?.content || ""} />;
    </div>
  );
}
