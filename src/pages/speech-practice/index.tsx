import { Paragraphs } from "@/components/SpeechPractice";
import React from "react";
import { useRouter } from "next/router";

export default function SpeechPractice() {
  const router = useRouter();
  return (
    <div className="bg-[url('/speech-practice/background/bg-speech-practice.svg')] bg-cover min-h-screen">
      <div className="flex flex-col justify-center items-center py-6">
        <div className="text-4xl font-bold ">Speech Practice</div>
        <div className="text-2xl ">Choose a paragraph to practice</div>
      </div>
      <div className="grid grid-cols-1 grid-rows-1 gap-4 mx-5 sm:mx-[5%] xl:mx-[20%]  py-[3rem] h-fit">
        {Paragraphs.map((p) => (
          <div className="col-span-1" key={p.id} onClick={() => router.push(`/speech-practice/${p.id}`)}>
            <div className="flex flex-wrap justify-start items-center  p-4 rounded-2xl cursor-pointer bg-white h-[50px]  text-2xl shadow-[0_8px_30px_rgb(0,0,0,0.12)]">
              <div className="truncate">{p.content}</div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
