import Speech from "@/hooks/microsoft/speech-service";
import { useRef, useState } from "react";

interface PronuncationProps {}

export default function Index({}: PronuncationProps) {
  const speech = new Speech();
  //   const [statusptt, setStatusSptt] = useState<string>("");
  const pronunciationScoreRef = useRef<any>(null);
  //   const [pronunciationScore, setPronunciationScore] = useState<any>(null);
  const microphoneOpenRef = useRef<boolean>(false);
  //   const [microphoneOpen, setMicrophoneOpen] = useState<boolean>(false);
  speech.pronunciationConfig("How old are you?");

  function handleMicToggle() {
    const isOpen = speech.toggleMic();
    // setMicrophoneOpen(isOpen);
    console.log("isOpen", microphoneOpenRef);
    microphoneOpenRef.current = isOpen;
  }

  function handleRecognize() {
    speech.recognize().then((pronunciationResult) => {
      console.log("pronunciation result", pronunciationResult);
      //   setPronunciationScore(pronunciationResult);
      console.log("result", pronunciationResult);
      pronunciationScoreRef.current = pronunciationResult;
    });
  }

  return (
    <>
      <button onClick={handleMicToggle}>
        {microphoneOpenRef ? "Close Microphone" : "Open Microphone"}
      </button>
      <br />
      {microphoneOpenRef && (
        <button onClick={handleRecognize}>Start Speech Recognition</button>
      )}

      {pronunciationScoreRef !== null && (
        <div>
          <h3>Pronunciation Score:</h3>
          <p>{JSON.stringify(pronunciationScoreRef)}</p>
        </div>
      )}
    </>
  );
}
