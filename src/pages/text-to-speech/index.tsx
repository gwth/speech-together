import { use, useState } from "react";

import { useMicrosoft } from "@/hooks/microsoft/v1";

// ui
import Container from "@mui/material/Container";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";

//types
import { TypeChangeEvent } from "@/utils/types";

//redux
import { useAppSelector } from "@/hooks/redux";
import { selectText } from "@/slices/microsoft/microsoftSlice";

export default function TextToSpeech() {
  const { TextToSpeech, SpeechToText, SpeechToTextPronunciation } =
    useMicrosoft();

  const [text, setText] = useState("");

  const handleTextChange = (e: TypeChangeEvent) => {
    setText(e.target.value);
  };

  const handleClickSubmit = () => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    TextToSpeech(text);
  };

  const handleClickRecordPronunciation = () => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    SpeechToTextPronunciation(text);
  };

  const handleClickRecord = () => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    SpeechToText();
  };

  return (
    <>
      <Container maxWidth="lg">
        <div className="text-xl">Text To Speech</div>
        <Grid container spacing={2}>
          <Grid item xs={10}>
            <TextField fullWidth size="small" onChange={handleTextChange} />
          </Grid>
          <Grid
            item
            xs={2}
            display="flex"
            justifyContent="center"
            alignItems="center">
            <Button variant="contained" onClick={handleClickSubmit}>
              submit
            </Button>
          </Grid>
        </Grid>
        <div className="text-xl">Speech To Text Pronunciation</div>
        <Grid container spacing={2}>
          <Grid item xs={2}>
            <Button
              variant="contained"
              onClick={handleClickRecordPronunciation}>
              Pronunciation
            </Button>
          </Grid>
        </Grid>
        <div className="text-xl">Speech To Text</div>
        <Grid container spacing={2}>
          <Grid item xs={2}>
            <Button variant="contained" onClick={handleClickRecord}>
              Record
            </Button>
          </Grid>
        </Grid>
      </Container>
    </>
  );
}
