import { useRouter } from "next/router";

// console.log(storedValue);
// export default function Page() {
//   return <p>Post: {storedValue}</p>;
// }
import Head from "next/head";
import React, { useEffect, useState } from "react";
import Nossr from "@mui/material/NoSsr";
import Container from "@mui/material/Container";
import Paper from "@mui/material/Paper";
import IconButton from "@mui/material/IconButton";
import MicIcon from "@mui/icons-material/Mic";
import VolumeUpIcon from "@mui/icons-material/VolumeUp";
import Grid from "@mui/material/Grid";
import RadioGroup from "@mui/material/RadioGroup";
import Radio from "@mui/material/Radio";
import FormControlLabel from "@mui/material/FormControlLabel";
import Button from "@mui/material/Button";
import Divider from "@mui/material/Divider";
import SendIcon from "@mui/icons-material/Send";
import Typography from "@mui/material/Typography";
import PopupDialog from "../../components/PopupDialog";

const paragraph: string = `Pichai: Hello!<br/>
Suda: Hi there! How are you?<br/>
Pichai: I'm good, thank you. How about you?<br/>
Suda: I'm fine, thanks. By the way, do you have any siblings?<br/>
Pichai: Yes, I have one sister. Her name is Pippa. She's younger than me. What about you?<br/>
Suda: Oh, that's nice! I have a brother named Mark. He's older than me by two years.<br/>
Pichai: That's cool. What does your brother do?<br/>
Suda: He works as a teacher at a local school. What about your sister?<br/>
Pichai: She's still studying. She's in high school right now.<br/>
Suda: Great! And what about your parents?<br/>
Pichai: My mom is a nurse and my dad works in an office. How about your parents?<br/>
Suda: My dad is a chef at a restaurant, and my mom works as a receptionist in a hotel.<br/>
Pichai: That sounds interesting. Do you all live together?<br/>
Suda: Yes, we do. We all live in a small apartment in the city.<br/>
Pichai: It must be fun living together as a family.<br/>
Suda: Absolutely! We have our moments, but overall, it's really nice.<br/>
Pichai: I agree. Family is important. Well, it was nice talking to you about our families.<br/>
Suda: Yes, it was. Let's catch up again soon. Have a good day!<br/>
Pichai: You too. Goodbye!<br/>
`;
const pathSoundBigQuestion = "/sounds/Going_to_the_Zoo.mp3";
const questionsListening = [
  {
    question: "Who suggested going to the zoo?",
    correctAnswer: "a",
    options: [
      { value: "a", text: "Tep" },
      { value: "b", text: "Ink" },
      { value: "c", text: "Both Tep and Ink" },
      { value: "d", text: "Another friend" },
    ],
    soundPath: "",
    selectAns: null,
  },
  {
    question: "What does Tep think of monkeys?",
    correctAnswer: "a",
    options: [
      { value: "a", text: "They're entertaining." },
      { value: "b", text: "They're scary." },
      { value: "c", text: "They're boring." },
      { value: "d", text: "They're loud." },
    ],
    soundPath: "",
    selectAns: null,
  },
  {
    question: "What is one animal they won't see at the zoo?",
    correctAnswer: "a",
    options: [
      { value: "a", text: "Tigers" },
      { value: "b", text: "Lions" },
      { value: "c", text: "Elephants" },
      { value: "d", text: "Monkeys" },
    ],
    soundPath: "",
    selectAns: null,
  },
];

const questionsReading = [
  {
    question: "What does Pichai’s mother do for a living?",
    correctAnswer: "a",
    options: [
      { value: "a", text: "Chef" },
      { value: "b", text: "Teacher" },
      { value: "c", text: "Nurse" },
      { value: "d", text: "Receptionist" },
    ],
    soundPath: "",
    selectAns: null,
  },
  {
    question: "Where does Suda’s family live?",
    correctAnswer: "a",
    options: [
      { value: "a", text: "City" },
      { value: "b", text: "Suburb" },
      { value: "c", text: "Village" },
      { value: "d", text: "Farm" },
    ],
    soundPath: "",
    selectAns: null,
  },
  {
    question: "What does Suda’s mother do for a living?",
    correctAnswer: "a",
    options: [
      { value: "a", text: "Receptionist " },
      { value: "b", text: "Teacher" },
      { value: "c", text: "Nurse" },
      { value: "d", text: "Chef" },
    ],
    soundPath: "",
    selectAns: null,
  },
];
function shuffleArray<T>(array: T[]): T[] {
  const shuffledArray = [...array];
  for (let i = shuffledArray.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [shuffledArray[i], shuffledArray[j]] = [shuffledArray[j], shuffledArray[i]];
  }
  return shuffledArray;
}

export default function Listening() {
  const router = useRouter();
  let storedValue: string | null = "";

  if (typeof window !== "undefined") {
    // Code that uses localStorage
    storedValue = localStorage.getItem("slug");
  }
  const [userAnswers, setUserAnswers] = useState<string[]>([]);

  // const [storedValue, setStoredValue] = useState<string>("");
  const [questions, setQuestion] = useState<any[]>(
    storedValue == "listening" ? questionsListening : questionsReading
  );
  const [paragraphTemp, setParagraphTemp] = useState<string>("");
  const [head, setHead] = useState<string>();
  const [open, setOpen] = useState(false);
  const [score, setScore] = useState<number>(0);
  const handleOpen = () => {
    sendAnswer();
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    if (userAnswers.length == questions.length) window.location.reload();
  };
  useEffect(() => {
    // setStoredValue(tempS !== null && tempS !== undefined ? tempS : "");
    console.log("useEffect", storedValue);
    setParagraphTemp(storedValue == "listening" ? "" : paragraph);
    console.log(storedValue == "listening");
    setQuestion(
      storedValue == "listening" ? questionsListening : questionsReading
    );
    setQuestion(shuffleArray(questions));
    questions.forEach((f) => {
      f.options = shuffleArray(f.options);
    });
    setHead(
      storedValue == "listening"
        ? "4.2.1 ข้อสอบทักษะการฟังภาษาอังกฤษ (ตอบแบบคำถามแบบตัวเลือก)"
        : "4.2.3 ข้อสอบทักษะการอ่านภาษาอังกฤษ (ตอบแบบคำถามแบบตัวเลือก)"
    );
  }, []);
  const handleRadioChange = (
    event: React.ChangeEvent<HTMLInputElement>,
    questionIndex: number
  ) => {
    const newAnswers = [...userAnswers];
    newAnswers[questionIndex] = event.target.value;
    setUserAnswers(newAnswers);
  };
  function sendAnswer() {
    console.log("sendAnswer");
    if (userAnswers.length == questions.length) {
      let scoreT: number = 0;
      questions.forEach((element, index) => {
        element.correctAnswer == userAnswers[index] ? scoreT++ : "";
      });
      // alert("คะแนนที่ได้ " + scoreT + " คะแนน");
      setScore(scoreT);
      // window.location.reload();
    }
  }
  const getParagraph = () => {
    if (storedValue != "listening") {
      return (
        <Grid xs={12} item>
          <Paper
            style={{
              backgroundColor: "rgba(242, 242, 242, 0.7)",
            }}
            className={
              " px-5 pt-3 pb-6 m-2  transition duration-150 rounded-xl  items-center bg-gray-200"
            }
          >
            <h1 className="text-black pl-5 w-[100%] text-center">
              Talking about Family
            </h1>
            <Typography
              className="text-xl text-md self-center text-left pl-5 leading-10 text-black"
              dangerouslySetInnerHTML={{ __html: paragraphTemp }}
            >
              {/* {paragraphTemp} */}
            </Typography>
          </Paper>
          <h2 className="text-black ps-3 pt-5">Choose the correct answer.</h2>
        </Grid>
      );
    }
  };

  const getAudioBigQuestion = () => {
    console.log("getAudioBigQuestion", storedValue);
    if (storedValue == "listening") {
      return (
        <Grid xs={12} item>
          <h2 className="text-black ps-3">
            Listen to the passage and choose the correct answer.
          </h2>
          <Paper
            style={{
              backgroundColor: "rgba(242, 242, 242, 0.7)",
            }}
            className={
              " px-5 pt-3 pb-6 m-2  transition duration-150 rounded-xl  items-center bg-gray-200"
            }
          >
            <Typography className="ms-2 pb-5 text-black text-xl text-center font-extrabold w-[100%]">
              Going to the Zoo
            </Typography>

            <div className="  bgaudio">
              <audio
                controls

                // style={{ backgroundColor: "rgba(0, 0, 0, 0.7)" }}
              >
                <source src={pathSoundBigQuestion} type="audio/mpeg"></source>
                Your browser does not support the audio element.
              </audio>
            </div>
          </Paper>
        </Grid>
      );
    }
  };
  const getAudio = (index: number) => {
    if (questions[index].soundPath != "") {
      return (
        <div className="text-left ps-5 pb-2">
          <audio controls className="disabled:opacity-75">
            <source src={questions[index].soundPath} type="audio/mpeg"></source>
            Your browser does not support the audio element.
          </audio>
        </div>
      );
    } else "";
  };
  const creatQuestion = () => {
    return questions.map((item, index: number) => {
      return (
        <Grid key={index} item xs={12}>
          <Paper
            style={{
              backgroundColor: "rgba(242, 242, 242, 0.7)",
            }}
            className={
              "p-2 m-2 pb-5  h-50 text-center transition duration-150 rounded-xl  items-center bg-gray-200"
            }
          >
            <div className="flex-column justify-start mb-5 ">
              <div className="text-md self-center text-left pl-5 leading-8 text-black">
                <h3>{item.question}</h3>
              </div>
              {getAudio(index)}
              <div className="text-md self-center text-left pl-5 leading-8">
                <RadioGroup
                  defaultValue="outlined"
                  name="radio-buttons-group"
                  onChange={(event) => handleRadioChange(event, index)}
                >
                  {/* {choice.text} */}
                  {item.options.map((choice: any, index2: number) => {
                    return (
                      <FormControlLabel
                        key={index2}
                        value={choice.value}
                        control={<Radio className="text-black" />}
                        label={choice.text}
                        className="text-black"
                      />
                    );
                  })}
                </RadioGroup>
              </div>
            </div>
          </Paper>
        </Grid>
      );
    });
  };
  function getText() {
    if (userAnswers.length == questions.length) {
      return "คะแนนที่ทำได้ " + score.toString() + " คะแนน";
    } else {
      return "กรุณาทำข้อสอบให้ครบ";
    }
  }
  return (
    <>
      <Head>
        <title>{head}</title>
      </Head>

      <Nossr>
        <Container maxWidth="lg">
          <Grid container spacing={2}>
            <Grid xs={12} item className=" flex justify-between my-10">
              {/* <div style={{ color: "white" }}>
                {storedValue == "listening"
                  ? "4.2.1 ข้อสอบทักษะการฟังภาษาอังกฤษ"
                  : "4.2.3 ข้อสอบทักษะการอ่านภาษาอังกฤษ "}{" "}
              </div> */}
              <Typography
                sx={{}}
                color="text.secondary"
                gutterBottom
                className="text-black font-semibold"
                variant="h5"
              >
                {head}
              </Typography>
              <Button
                onClick={handleOpen}
                variant="contained"
                startIcon={<SendIcon />}
              >
                Send
              </Button>
            </Grid>
            <Divider
              sx={{
                bgcolor: "rgba(140, 140, 140, 1)",
                width: "100%",
              }}
            />
            {getParagraph()}
            {getAudioBigQuestion()}

            {creatQuestion()}
          </Grid>
        </Container>
      </Nossr>
      <PopupDialog
        open={open}
        onClose={handleClose}
        // text={"คะแนนที่ทำได้ " + {{...score.toString()}} + "คะแนน"}
        text={getText()}
      />
    </>
  );
}

// export default listening;

// {
//   questions.map((item, index: number) => {
//     return (
//       <Grid key={index} item xs={12}>
//         <Paper
//           style={{
//             backgroundColor: "rgba(0, 0, 0, 0.7)",
//           }}
//           className={
//             "p-2 m-2 pb-5  h-50 text-center transition duration-150 rounded-xl  items-center bg-gray-200"
//           }
//         >
//           <div className="flex-column justify-start mb-5 ">
//             <div className="text-md self-center text-left pl-5 leading-8 text-black">
//               <h3>{item.question}</h3>
//             </div>
//             {getAudio(index)}
//             <div className="text-md self-center text-left pl-5 leading-8">
//               <RadioGroup
//                 defaultValue="outlined"
//                 name="radio-buttons-group"
//                 onChange={(event) => handleRadioChange(event, index)}
//               >
//                 {/* {choice.text} */}
//                 {item.options.map((choice: any, index2: number) => {
//                   return (
//                     <FormControlLabel
//                       key={index2}
//                       value={choice.value}
//                       control={<Radio className="text-black" />}
//                       label={choice.text}
//                       className="text-black"
//                     />
//                   );
//                 })}
//               </RadioGroup>
//             </div>
//           </div>
//         </Paper>
//       </Grid>
//     );
//   });
// }
