import Container from "@mui/material/Container";

import Nossr from "@mui/material/NoSsr";
import React, { useState } from "react";
import Paper from "@mui/material/Paper";
import IconButton from "@mui/material/IconButton";
import MicIcon from "@mui/icons-material/Mic";
import VolumeUpIcon from "@mui/icons-material/VolumeUp";
import Grid from "@mui/material/Grid";
import { useMicrosoft } from "@/hooks/microsoft/v2";
//css
import styles from "@/styles/conversation.module.css";
import { IPronResult, IPronResultVocab } from "@/hooks/microsoft/types";

import CircularProgressWithLabel from "@/components/ProgressScore";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
export default function vocabulary() {
  const [currentIndex, setCurrentIndex] = useState(99);
  const [pronResult, setPronResult] = React.useState<IPronResultVocab[]>([]);
  const [renderText, setRenderText] = React.useState<string>("");
  const mockVocab: string[] = [
    "Food",
    "energy",
    "strong",
    "meal",
    "breakfast",
    "The body needs food, especially in the morning. Why? It’s because we sleep all night and we don’t eat. In the morning, we need energy. Breakfast gives us energy. When we have energy, we can learn more. We feel happy and strong. In the morning, we have to hurry but don’t forget breakfast! It’s the most important meal of your day.",
  ];
  const { TextToSpeech, Pronunciation, status } = useMicrosoft();
  function clickVolumn(text: string) {
    TextToSpeech(text);
  }
  function cc() {
    // console.log("cc", pronResult[pronResult.length - 1].detailResult);
    // if (pronResult[pronResult.length - 1].detailResult.Words) {
    //   console.log("cc2", pronResult[pronResult.length - 1].detailResult.Words);
    // }
    console.log(mockVocab[5].length);
    // setCurrentIndex(3);
    // setPronResult([
    //   ...pronResult,
    //   {
    //     accuracyScore: 70,
    //     fluencyScore: 50,
    //     pronunciationScore: 90,
    //     completenessScore: 30,
    //     index: 3,
    //   },
    // ]);
  }
  async function clickMic(text: string, index: number) {
    console.log("click mic");
    const pronResultFromFunc = await Pronunciation(text);
    console.log("mm", pronResultFromFunc);
    if (pronResultFromFunc) {
      setPronResult([
        ...pronResult,
        {
          accuracyScore: pronResultFromFunc.accuracyScore,
          fluencyScore: pronResultFromFunc.fluencyScore,
          pronunciationScore: pronResultFromFunc.pronunciationScore,
          completenessScore: pronResultFromFunc.completenessScore,
          index: index,
          detailResult: pronResultFromFunc.detailResult,
        },
      ]);
      //console.log("xx", pronResult);
    } else {
    }
  }
  const handleClickSetCurrent = (index: number) => {
    setCurrentIndex(index);
  };

  const loadingMicAndAudio = (index: number) => {
    if (currentIndex === index && status === "processing") {
      return <div className={`${styles["dot-flashing"]} self-center ms-10`} />;
    } else if (currentIndex === index && status === "error") {
      return (
        <div className="text-md self-center grow text-red-500 font-semibold flex justify-end">
          Try again
        </div>
      );
    } else "";
  };
  function getColor(value: number): string {
    switch (true) {
      case value >= 80:
        return "green";
      case value >= 50:
        return "orange";
      case value < 50:
        return "red";
    }
    return styles["dot-flashing"];
  }
  const showScore = (index: number) => {
    let x = pronResult.filter((f) => f.index === index);
    if (x.length > 0) {
      //console.log("ddd", x);

      //   return <div>accuracyScore {x[x.length - 1].accuracyScore} </div>;
      return (
        <div>
          <h4 className="text-left pl-5 pb-2">Assessment Result</h4>
          <Grid container>
            <Grid item xs={6} md={5} lg={3} className=" justify-center">
              <CircularProgressWithLabel
                value={x[x.length - 1].pronunciationScore}
                size={60}
                sx={{
                  color: getColor(x[x.length - 1].pronunciationScore),
                }}
              ></CircularProgressWithLabel>
              <p>Pronunciation Score</p>
            </Grid>
            {/* <Grid item xs={6} md={5} lg={3}>
              <CircularProgressWithLabel
                value={x[x.length - 1].accuracyScore}
                size={60}
                sx={{ color: getColor(x[x.length - 1].accuracyScore) }}
              ></CircularProgressWithLabel>
              <p>AccuracyScore</p>
            </Grid>
            <Grid item xs={6} md={5} lg={3}>
              <CircularProgressWithLabel
                value={x[x.length - 1].fluencyScore}
                size={60}
                sx={{ color: getColor(x[x.length - 1].fluencyScore) }}
              ></CircularProgressWithLabel>
              <p>FluencyScore</p>
            </Grid>
            <Grid item xs={6} md={5} lg={3}>
              <CircularProgressWithLabel
                value={x[x.length - 1].completenessScore}
                size={60}
                sx={{ color: getColor(x[x.length - 1].completenessScore) }}
              ></CircularProgressWithLabel>
              <p>CompletenessScore</p>
            </Grid> */}
          </Grid>
        </div>
      );
    } else "";
  };
  function getColorText(value: string): string {
    switch (value) {
      case "None":
        return "green";
      case "Mispronunciation":
        return "red";
    }
    return styles["dot-flashing"];
  }
  const showText = (index: number) => {
    let x = pronResult.filter((f) => f.index === index);
    if (x.length > 0) {
      let dataTemp = x[x.length - 1].detailResult.Words;
      // console.log("dataTemp", dataTemp);
      if (dataTemp.length > 0) {
        return (
          <div className="text-start pl-5 text-lg font-bold ">
            {dataTemp.map((item: any, index: number) => (
              <a
                // sx={{
                //   color: getColor(item.PronunciationAssessment.ErrorType),
                // }}
                style={{
                  color: getColorText(item.PronunciationAssessment.ErrorType),
                }}
                key={index}
              >
                {item.Word}{" "}
              </a>
            ))}
          </div>
        );
      }
    } else "";
  };
  return (
    <>
      <Nossr>
        <Container maxWidth="lg">
          <Grid container spacing={2}>
            <div className="text-4xl font-bold flex justify-center my-10">
              Vocabulary & Sentence
            </div>

            {mockVocab.map((item: string, index: number) => {
              return (
                <Grid key={index} item xs={12}>
                  <Paper
                    className={
                      "p-2 m-2 pb-5  h-50 text-center transition duration-150 ease-in-out hover:scale-105 rounded-xl  items-center bg-gray-200"
                    }
                  >
                    <div className="flex justify-start mb-5 ">
                      <div className="text-md self-center text-left pl-5 leading-8">
                        <h3>{item}</h3>
                      </div>
                      <div className="flex ms-5">
                        <IconButton
                          onClick={() => {
                            handleClickSetCurrent(index);
                            clickVolumn(item);
                          }}
                        >
                          <VolumeUpIcon className="cursor-pointer" />
                        </IconButton>

                        <IconButton
                          onClick={async () => {
                            handleClickSetCurrent(index);
                            await clickMic(item, index);
                          }}
                        >
                          <MicIcon className="cursor-pointer" />
                        </IconButton>
                        {/* <IconButton
                          onClick={() => {
                            cc();
                          }}
                        >
                          <MicIcon className="cursor-pointer" />
                        </IconButton> */}
                      </div>
                    </div>
                    {loadingMicAndAudio(index)}
                    {showScore(index)}
                    {showText(index)}
                  </Paper>
                </Grid>
              );
            })}
          </Grid>
        </Container>
      </Nossr>
    </>
  );
}
