import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface MicrosoftState {
  text: string;
}

const initialState: MicrosoftState = {
  text: "",
};

export const microsoftSlice = createSlice({
  name: "microsoft",
  initialState,
  reducers: {
    setText: (state, action: PayloadAction<string>) => {
      state.text = action.payload;
    },
  },
});

export const { setText } = microsoftSlice.actions;

export const selectText = (state: { microsoft: MicrosoftState }) =>
  state.microsoft.text;

export default microsoftSlice.reducer;
