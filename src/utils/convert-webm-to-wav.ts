import ffmpegPath from "@ffmpeg-installer/ffmpeg";
import ffmpeg from "fluent-ffmpeg";
import path from "path";

ffmpeg.setFfmpegPath(ffmpegPath.path);

/**
 * Convert a WebM Blob to WAV format and save it to a directory.
 * @param inputBlob - The input WebM Blob.
 * @param outputDirectory - Path to the directory where the WAV file will be saved.
 */
export async function convertWebMBlobToWavAndSave(source: string, outputDirectory: string): Promise<string> {
  return new Promise<string>(async (resolve, reject) => {
    const outputFileName = "output.wav";
    const outputPath = path.join(outputDirectory, outputFileName);

    // const inputBuffer = Buffer.from(await inputBlob.arrayBuffer());

    ffmpeg()
      .input(source)
      .audioCodec("pcm_s16le")
      .audioChannels(1)
      .toFormat("wav")
      .on("end", () => {
        resolve(outputFileName);
      })
      .on("error", (err) => {
        reject(err);
      })
      .save(outputPath);
  });
}
